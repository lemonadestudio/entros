<?php

namespace Ls\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;

class PhotoType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $entity = $event->getData();
            $form = $event->getForm();

            if (!$entity || null === $entity->getId()) {
                $form->add('file', 'file', array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wybierz zdjęcie'
                        )),
                        new Image(array(
                            'minHeight' => 32,
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 32px',
                        ))
                    )
                ));
            } else {
                $form->add('file', 'file', array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new Image(array(
                            'minHeight' => 32,
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 32px',
                        ))
                    )
                ));
            }
            $form->add('description', 'textarea', array(
                'label' => 'Opis'
            ));
        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\MainBundle\Entity\Photo',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_gallery_photo';
    }
}
