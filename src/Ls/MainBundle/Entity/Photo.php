<?php

namespace Ls\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Ls\MainBundle\Utils\Tools;

/**
 * GalleryPhoto
 * @ORM\Table(name="gallery_photo")
 * @ORM\Entity
 */
class Photo {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    protected $certWidth    = 97;
    protected $certHeight   = 97;
    protected $listWidth    = 137;
    protected $listHeight   = 115;
    protected $detailWidth  = 275;
    protected $detailHeight = 231;
    protected $mainWidth    = 400;
    protected $mainHeight   = 346;


    /**
     * @ORM\ManyToOne(
     *     targetEntity="Gallery",
     *     inversedBy="photos"
     * )
     * @ORM\JoinColumn(
     *     name="gallery_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @var \Ls\MainBundle\Entity\Gallery
     */
    private $gallery;


    private $file;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Photo
     */
    public function setPhoto($photo) {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * Set photo
     *
     * @param string $description
     * @return Photo
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return Icon
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement() {
        return $this->arrangement;
    }


    /**
     * Set gallery
     *
     * @param \Ls\MainBundle\Entity\Gallery $gallery
     * @return Photo
     */
    public function setGallery(Gallery $gallery = null) {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \Ls\MainBundle\Entity\Gallery
     */
    public function getGallery() {
        return $this->gallery;
    }

    public function __toString() {
        if (is_null($this->getPhoto())) {
            return 'NULL';
        }
        return $this->getPhoto();
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFile(UploadedFile $file = null) {
        $this->deletePhoto();
        $this->file = $file;
        if (empty($this->photo)) {
            $this->setPhoto('empty');
        } else {
            $this->setPhoto('');
        }
    }

    public function getFile() {
        return $this->file;
    }

    public function deletePhoto() {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            if (file_exists($filename)) {
                @unlink($filename);
            }
        }
    }

    public function getPhotoWebPath() {
        if (null === $this->photo || empty($this->photo)) {
            return false;
        } else {
            return '/' . $this->getUploadDir() . '/' . $this->photo;
        }
    }

    public function getPhotoAbsolutePath() {
        if (null === $this->photo || empty($this->photo)) {
            return false;
        } else {
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
        }
    }

    public function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    public function getTempDir() {
        return  __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'upload/tempfiles';
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/galleries';
    }

    public function upload($main = false) {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->createThumb();
        
        unset($this->file);
    }

    public function createThumb() {

        $sFileName = $this->getPhoto();

        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameL = Tools::thumbName($sSourceName, '_l');
        $aThumbSizeL = $this->getThumbSize('list');
        $thumb->adaptiveResize($aThumbSizeL['width'] + 2, $aThumbSizeL['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeL['width'], $aThumbSizeL['height']);
        $thumb->save($sThumbNameL);

        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameD = Tools::thumbName($sSourceName, '_d');
        $aThumbSizeD = $this->getThumbSize('detail');
        $thumb->adaptiveResize($aThumbSizeD['width'] + 2, $aThumbSizeD['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeD['width'], $aThumbSizeD['height']);
        $thumb->save($sThumbNameD);

        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameM = Tools::thumbName($sSourceName, '_m');
        $aThumbSizeM = $this->getThumbSize('main');
        $thumb->adaptiveResize($aThumbSizeM['width'] + 2, $aThumbSizeM['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeM['width'], $aThumbSizeM['height']);
        $thumb->save($sThumbNameM);

        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameM = Tools::thumbName($sSourceName, '_c');
        $aThumbSizeM = $this->getThumbSize('cert');
        $thumb->adaptiveResize($aThumbSizeM['width'] + 2, $aThumbSizeM['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeM['width'], $aThumbSizeM['height']);
        $thumb->save($sThumbNameM);

    }

    public function getThumbWebPath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'list':
                    $sThumbName = Tools::thumbName($this->photo, '_l');
                    break;
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
                case 'main':
                    $sThumbName = Tools::thumbName($this->photo, '_m');
                    break;
                case 'cert':
                    $sThumbName = Tools::thumbName($this->photo, '_c');
                    break;
            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'list':
                    $sThumbName = Tools::thumbName($this->photo, '_l');
                    break;
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
                case 'main':
                    $sThumbName = Tools::thumbName($this->photo, '_m');
                    break;
                case 'cert':
                    $sThumbName = Tools::thumbName($this->photo, '_c');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

     public function getThumbSize($type) {
        $size = array();
        switch ($type) {
            case 'list':
                $size['width'] = $this->listWidth;
                $size['height'] = $this->listHeight;
                break;
            case 'detail':
                $size['width'] = $this->detailWidth;
                $size['height'] = $this->detailHeight;
                break;
            case 'main':
                $size['width'] = $this->mainWidth;
                $size['height'] = $this->mainHeight;
                break;
            case 'cert':
                $size['width'] = $this->certWidth;
                $size['height'] = $this->certHeight;
                break;
        }
        return $size;
    }

    public function Thumb($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = \PhpThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }
}