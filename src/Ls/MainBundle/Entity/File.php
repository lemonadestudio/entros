<?php

namespace Ls\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Ls\MainBundle\Utils\Tools;

/**
 * File
 * @ORM\Table(name="file")
 * @ORM\Entity
 */
class File {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $filename;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Product",
     *     inversedBy="pliki"
     * )
     * @ORM\JoinColumn(
     *     name="product_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @var \Ls\MainBundle\Entity\Product
     */
    private $product;

    /**
     * Constructor
     */
    public function __construct() {
        $this->created_at = new \DateTime();
    }

    private $file;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return Icon
     */
    public function setFilename($filename) {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename() {
        return $this->filename;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return Icon
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Set product
     *
     * @param \Ls\MainBundle\Entity\Product $product
     * @return Icon
     */
    public function setProduct(Product $product = null) {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Ls\MainBundle\Entity\Gallery
     */
    public function getProduct() {
        return $this->product;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return File
     */
    public function setCreatedAt($createdAt) {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    public function __toString() {
        if (is_null($this->getFilename())) {
            return 'NULL';
        }
        return $this->getFilename();
    }

    public function setFile(UploadedFile $file = null) {
        $this->deleteFilename();
        $this->file = $file;
        if (empty($this->photo)) {
            $this->setFilename('empty');
        } else {
            $this->setFilename('');
        }
    }

    public function getFile() {
        return $this->file;
    }

    public function deleteFilename() {
        if (!empty($this->filename)) {
            $filename = $this->getFilenameAbsolutePath();
            if (file_exists($filename)) {
                @unlink($filename);
            }
        }
    }

    public function getFileWebPath() {
        if (null === $this->filename || empty($this->filename)) {
            return false;
        } else {
            return '/' . $this->getUploadDir() . '/' . $this->filename;
        }
    }

    public function getFileAbsolutePath() {
        if (null === $this->filename || empty($this->filename)) {
            return false;
        } else {
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->filename;
        }
    }

    public function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/file';
    }

    public function upload($main = false) {
        // die('fuck');
        if (null === $this->file) {
            return;
        }
        // var_dump($this->getFile()->getClientOriginalName());
        // die('bang');
        $sFileName = $this->getFile();

        $ext = $this->file->guessExtension();

        $sFileName = $this->getFile();

        $this->setFilename($sFileName->getClientOriginalName());

        // var_dump($this);
        // die('boom');

        $this->file->move($this->getUploadRootDir(), $sFileName->getClientOriginalName());

        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;
    }

    public function checkFileExt() {
        $ext = pathinfo($this->getFilename(), PATHINFO_EXTENSION);
        return $ext;
    }
    public function checkFileSize() {
        $size = filesize($this->getFileAbsolutePath());
        $unit = 'B';
        if(1024 < $size) {
            $size = $size / 1024;
            $unit = 'KB';
            if(1024 < $size) {
                $size = $size / 1024;
                $unit = 'MB';
            }
        }
        $size = number_format($size, 2, ',', '');
        $result = ['size' => $size, 'unit' => $unit];
        return $result;
    }
}
