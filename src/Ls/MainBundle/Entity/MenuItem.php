<?php

namespace Ls\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * MenuItem
 * @ORM\Table(name="menu_item")
 * @ORM\Entity
 */
class MenuItem {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $route;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $route_parameters;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $onclick;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $blank;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\OneToMany(
     *   targetEntity="MenuItem",
     *   mappedBy="parent"
     * )
     * @ORM\OrderBy({"arrangement" = "ASC"})
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children;

    /**
     * @ORM\ManyToOne(
     *   targetEntity="MenuItem",
     *   inversedBy="children"
     * )
     * @var \Ls\MainBundle\Entity\MenuItem
     */
    private $parent;

    /**
     * Constructor
     */
    public function __construct() {
        $this->blank = false;
        $this->children = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return MenuItem
     */
    public function setLocation($location) {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return MenuItem
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return MenuItem
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set route
     *
     * @param string $route
     * @return MenuItem
     */
    public function setRoute($route) {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string 
     */
    public function getRoute() {
        return $this->route;
    }

    /**
     * Set route_parameters
     *
     * @param string $route_parameters
     * @return MenuItem
     */
    public function setRouteParameters($route_parameters) {
        $this->route_parameters = $route_parameters;

        return $this;
    }

    /**
     * Get route_parameters
     *
     * @return string 
     */
    public function getRouteParameters() {
        return $this->route_parameters;
    }

    public function getRouteParametersArray() {
    	return (array)json_decode($this->getRouteParameters());
    }

    /**
     * Set url
     *
     * @param string $url
     * @return MenuItem
     */
    public function setUrl($url) {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * Set onclick
     *
     * @param string $onclick
     * @return MenuItem
     */
    public function setOnclick($onclick) {
        $this->onclick = $onclick;

        return $this;
    }

    /**
     * Get onclick
     *
     * @return string 
     */
    public function getOnclick() {
        return $this->onclick;
    }

    /**
     * Set blank
     *
     * @param boolean $blank
     * @return MenuItem
     */
    public function setBlank($blank) {
        $this->blank = $blank;

        return $this;
    }

    /**
     * Get blank
     *
     * @return boolean
     */
    public function getBlank() {
        return $this->blank;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return MenuItem
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer 
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Add children
     *
     * @param \Ls\MainBundle\Entity\MenuItem $children
     * @return MenuItem
     */
    public function addChildren(MenuItem $children) {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \Ls\MainBundle\Entity\MenuItem $children
     */
    public function removeChildren(MenuItem $children) {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \Ls\MainBundle\Entity\MenuItem $parent
     * @return MenuItem
     */
    public function setParent(MenuItem $parent = null) {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Ls\MainBundle\Entity\MenuItem
     */
    public function getParent() {
        return $this->parent;
    }

    public function __toString() {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }
        return $this->getTitle();
    }
}