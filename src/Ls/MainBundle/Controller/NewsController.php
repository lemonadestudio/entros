<?php

namespace Ls\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * News controller.
 *
 */
class NewsController extends Controller {

    /**
     * Lists all News entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $limit = $em->getRepository('LsMainBundle:Setting')->findOneByLabel('limit_news')->getValue();

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsMainBundle:News', 'a')
            ->where('a.published_at <= :today')
            ->setParameter('today', $today)
            ->orderBy('a.published_at', 'DESC')
            ->getQuery()
            ->getResult();

        $page = $this->get('request')->query->get('page', 1);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities, $page, $limit
        );

        return $this->render('LsMainBundle:News:index.html.twig', array(
            'entities' => $pagination,
        ));
    }

    /**
     * Finds and displays a News entity.
     *
     */
    public function showAction($slug) {
        $today = new \DateTime();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:News', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find News entity.');
        }

        $news = $em->createQueryBuilder()
            ->select('p')
            ->where('p.published_at <= :today')
            ->from('LsMainBundle:News', 'p')
            ->setParameter('today', $today)
            ->getQuery()
            ->getResult();

        $ids = array();
        foreach ($news as $info) {
            $ids[] = $info->getId();
        }

        if (2 < count($ids)) {
            $rev_news = array_reverse($news);
            if ($entity->getId() == $news[0]->getId()) {
                $show_news = array(
                    $news[2],
                    $news[1]
                );
            } elseif ($entity->getId() == $rev_news[0]->getId()) {
                $show_news = array(
                    $rev_news[1],
                    $rev_news[2]
                );
            } else {
                foreach ($news as $key => $value) {
                    if ($entity->getId() == $value->getId()) {
                        $show_news = array(
                            $news[$key + 1],
                            $news[$key - 1]
                        );
                    }
                }
            }
        } else {
            $show_news = $news;
        }

        return $this->render('LsMainBundle:News:show.html.twig', array(
            'entity' => $entity,
            'news' => $show_news
        ));
    }
}
