<?php

namespace Ls\AdminBundle\Controller;

use Ls\AdminBundle\Form\IconType;
use Ls\AdminBundle\Form\IconPageType;
use Ls\MainBundle\Entity\Icon;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class PageIconController extends Controller {
    private $pager_limit_name = 'admin_page_icon_pager_limit';

    public function indexAction($pageId) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $session = $this->container->get('session');

        $pageEntity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Page', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $pageId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $pageEntity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsMainBundle:Icon', 'e')
            ->where('e.page = :page')
            ->setParameter('page', $pageId)
            ->orderBy('e.arrangement', 'asc')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit
        );
        $entities->setTemplate('LsAdminBundle::paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_page_icon'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Podstrony', $this->get('router')->generate('ls_admin_page'));
        $breadcrumbs->addItem($pageEntity->__toString(), $this->get('router')->generate('ls_admin_page_edit', array('id' => $pageEntity->getId())));
        $breadcrumbs->addItem('Logotypy', $this->get('router')->generate('ls_admin_page_icon', array('pageId' => $pageId)));

        return $this->render('LsAdminBundle:PageIcon:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'page_id' => $pageEntity->getId(),
            'entities' => $entities,
        ));
    }

    public function newAction($pageId) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $pageEntity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Page', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $pageId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $pageEntity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $entity = new Icon();
        $entity->setPage($pageEntity);

        $form = $this->createForm(new IconPageType(), $entity, array(
            'action' => $this->generateUrl('ls_admin_page_icon_new', array('pageId' => $pageId)),
            'method' => 'POST'
        ));
        $form->add('submit', 'submit', array('label' => 'Zapisz'));
        $form->add('submit_and_list', 'submit', array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', 'submit', array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            if (null !== $entity->getFile()) {
                $sFileName = uniqid('icon-image-') . '.' . $entity->getFile()->guessExtension();
                $entity->setPhoto($sFileName);
                $entity->upload();
            }
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie logotypu zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_page_icon_edit', array('pageId' => $pageId, 'id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_page_icon', array('pageId' => $pageId)));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_page_icon_new', array('pageId' => $pageId)));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Podstrony', $this->get('router')->generate('ls_admin_page'));
        $breadcrumbs->addItem($pageEntity->__toString(), $this->get('router')->generate('ls_admin_page_edit', array('id' => $pageEntity->getId())));
        $breadcrumbs->addItem('Logotypy', $this->get('router')->generate('ls_admin_page_icon', array('pageId' => $pageId)));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_page_icon_new', array('pageId' => $pageId)));

        return $this->render('LsAdminBundle:PageIcon:new.html.twig', array(
            'form' => $form->createView(),
            'page_id' => $pageId
        ));
    }

    public function editAction($pageId, $id) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $pageEntity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Page', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $pageId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $pageEntity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $entity = $em->getRepository('LsMainBundle:Icon')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Icon entity.');
        }

        $form = $this->createForm(new IconPageType(), $entity, array(
            'action' => $this->generateUrl('ls_admin_page_icon_edit', array('pageId' => $pageId, 'id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', 'submit', array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', 'submit', array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            if (null !== $entity->getFile()) {
                $sFileName = uniqid('icon-image-') . '.' . $entity->getFile()->guessExtension();
                $entity->setPhoto($sFileName);
                $entity->upload();
            }
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja logotypu zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_page_icon_edit', array('pageId' => $pageId, 'id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_page_icon', array('pageId' => $pageId)));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Podstrony', $this->get('router')->generate('ls_admin_page'));
        $breadcrumbs->addItem($pageEntity->__toString(), $this->get('router')->generate('ls_admin_page_edit', array('id' => $pageEntity->getId())));
        $breadcrumbs->addItem('Logotypy', $this->get('router')->generate('ls_admin_page_icon', array('pageId' => $pageId)));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_page_icon_edit', array('pageId' => $pageId, 'id' => $entity->getId())));

        return $this->render('LsAdminBundle:PageIcon:edit.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
            'page_id' => $pageId
        ));
    }

    public function deleteAction($pageId, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsMainBundle:Icon')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Icon entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie logotypu zakończone sukcesem.');

        return new Response('OK');
    }

    private function getMaxKolejnosc($page) {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsMainBundle:Icon', 'c')
            ->where('c.page = :page')
            ->setParameter('page', $page->getId())
            ->getQuery();

        $total = $query->getSingleScalarResult();
        return $total + 1;
    }

    public function moveDownAction($pageId, $id) {
        $em = $this->getDoctrine()->getManager();

        $pageEntity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Page', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $pageId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $pageEntity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $entity = $em->getRepository('LsMainBundle:Icon')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Icon entity.');
        }

        $max = $this->getMaxKolejnosc($pageEntity);
        $old_kolejnosc = $entity->getArrangement();
        $new_kolejnosc = $old_kolejnosc + 1;
        if ($new_kolejnosc < $max) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsMainBundle:Icon', 'c')
                ->where('c.arrangement = :arrangement')
                ->andWhere('c.page = :page')
                ->setParameter('arrangement', $new_kolejnosc)
                ->setParameter('page', $pageEntity->getId())
                ->getQuery();

            $old_entity_id = $query->getSingleScalarResult();
            $old_entity = $em->getRepository('LsMainBundle:Icon')->findOneById($old_entity_id);
            $old_entity->setArrangement(0);
            $em->persist($old_entity);
            $em->flush();
            $entity->setArrangement($new_kolejnosc);
            $em->persist($entity);
            $em->flush();
            $old_entity->setArrangement($old_kolejnosc);
            $em->persist($old_entity);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->add('success', 'Przeniesienie logotypu do dołu zakończone sukcesem.');

        return new Response('OK');
    }

    public function moveUpAction($pageId, $id) {
        $em = $this->getDoctrine()->getManager();

        $pageEntity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Page', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $pageId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $pageEntity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $entity = $em->getRepository('LsMainBundle:Icon')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Icon entity.');
        }

        $old_kolejnosc = $entity->getArrangement();
        $new_kolejnosc = $old_kolejnosc - 1;
        if ($new_kolejnosc > 0) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsMainBundle:Icon', 'c')
                ->where('c.arrangement = :arrangement')
                ->andWhere('c.page = :page')
                ->setParameter('arrangement', $new_kolejnosc)
                ->setParameter('page', $pageEntity->getId())
                ->getQuery();

            $old_entity_id = $query->getSingleScalarResult();
            $old_entity = $em->getRepository('LsMainBundle:Icon')->findOneById($old_entity_id);
            $old_entity->setArrangement(0);
            $em->persist($old_entity);
            $em->flush();
            $entity->setArrangement($new_kolejnosc);
            $em->persist($entity);
            $em->flush();
            $old_entity->setArrangement($old_kolejnosc);
            $em->persist($old_entity);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->add('success', 'Przeniesienie logotypu do góry zakończone sukcesem.');

        return new Response('OK');
    }

    public function batchAction($pageId) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $pageEntity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Page', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $pageId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $pageEntity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
            $breadcrumbs->addItem('Podstrony', $this->get('router')->generate('ls_admin_page'));
            $breadcrumbs->addItem($pageEntity->__toString(), $this->get('router')->generate('ls_admin_page_edit', array('id' => $pageEntity->getId())));
            $breadcrumbs->addItem('Logotypy', $this->get('router')->generate('ls_admin_page_icon', array('pageId' => $pageId)));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_page_icon_batch', array('pageId' => $pageId)));

            return $this->render('LsAdminBundle:PageIcon:batch.html.twig', array(
                'page_id' => $pageId,
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_page_icon', array('pageId' => $pageId)));
        }
    }

    public function batchExecuteAction($pageId) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $batchSize = 50;
                    $index = 0;
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsMainBundle:Icon', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        if (($index % $batchSize) === 0) {
                            $em->flush();
                            $em->clear();
                        }
                        ++$index;
                    }
                    $em->flush();
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_page_icon', array('pageId' => $pageId)));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_page_icon', array('pageId' => $pageId)));
        }
    }

    public function setLimitAction($pageId) {
        $request = $this->get('request');
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
}
