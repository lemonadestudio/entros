<?php

namespace Ls\AdminBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\MainBundle\Entity\MenuItem;

class MenuUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'preUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof MenuItem) {
            if (null === $entity->getArrangement()) {
                $location = $entity->getLocation();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsMainBundle:MenuItem', 'c')
                    ->where('c.location = :location')
                    ->setParameter('location', $location)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof MenuItem) {
            if (null === $entity->getArrangement()) {
                $location = $entity->getLocation();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsMainBundle:MenuItem', 'c')
                    ->where('c.location = :location')
                    ->setParameter('location', $location)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof MenuItem) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();
                $location = $entity->getLocation();

                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsMainBundle:MenuItem', 'c')
                    ->where('c.arrangement > :arrangement')
                    ->andWhere('c.location = :location')
                    ->setParameter('arrangement', $arrangement)
                    ->setParameter('location', $location)
                    ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['type'] = 'MenuItem';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
        }
    }
}