<?php

namespace Ls\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class OpiniaType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', null, array(
            'label' => 'Imię:',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        // $builder->add('age', null, array(
        //     'label' => 'Wiek:'
        // ));
        $builder->add('title', null, array(
            'label' => 'Tytuł:'
        ));
        $builder->add('content', null, array(
            'label' => 'Treść:',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('created_at', 'datetime', array(
            'label' => 'Data dodania',
            'widget' => 'single_text',
            'format' => 'dd.MM.yyyy HH:mm'
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\MainBundle\Entity\Opinia',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_opinia';
    }
}
