<?php

namespace Ls\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller {
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $today = new \DateTime();

        $pages = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:Page', 'a')
            ->getQuery()
            ->getResult();

        $opinia = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:Opinia', 'a')
            ->where('a.accepted = 1')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();

        $news = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:News', 'a')
            ->where('a.published_at <= :today')
            ->setParameter('today', $today->format('Y-m-d 00:00:00'))
            ->setMaxResults(2)
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();

        $differences = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:Difference', 'a')
            ->getQuery()
            ->getResult();

        $produkt = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:Product', 'a')
            ->orderBy('a.arrangement', 'ASC')
            ->setMaxResults(8)
            ->getQuery()
            ->getResult();

        $galleries = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:Zespol', 'a')
            ->where('a.gallery IS NOT NULL')
            ->setMaxResults(2)
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('LsMainBundle:Default:index.html.twig', array(
            'pages' => $pages,
            'opinia' => $opinia,
            'products' => $produkt,
            'differences' => $differences,
            'gall' => $galleries,
            'news' => $news
        ));
    }
}
