<?php

namespace Ls\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Oferta
 * @ORM\Table(name="oferta")
 * @ORM\Entity
 */
class Oferta {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $old_slug;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $content_short_generate;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content_short;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seo_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_description;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    /**
     * Constructor
     */
    public function __construct() {
        $this->content_short_generate = true;
        $this->seo_generate = true;
        $this->created_at = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Oferta
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Oferta
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set old_slug
     *
     * @param string $old_slug
     * @return Oferta
     */
    public function setOldSlug($old_slug) {
        $this->old_slug = $old_slug;

        return $this;
    }

    /**
     * Get old_slug
     *
     * @return string
     */
    public function getOldSlug() {
        return $this->old_slug;
    }

    /**
     * Set content_short_generate
     *
     * @param boolean $contentShortGenerate
     * @return Oferta
     */
    public function setContentShortGenerate($contentShortGenerate) {
        $this->content_short_generate = $contentShortGenerate;

        return $this;
    }

    /**
     * Get content_short_generate
     *
     * @return boolean
     */
    public function getContentShortGenerate() {
        return $this->content_short_generate;
    }

    /**
     * Set content_short
     *
     * @param string $contentShort
     * @return Oferta
     */
    public function setContentShort($contentShort) {
        $this->content_short = $contentShort;

        return $this;
    }

    /**
     * Get content_short
     *
     * @return string
     */
    public function getContentShort() {
        return $this->content_short;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Oferta
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set seo_generate
     *
     * @param boolean $seoGenerate
     * @return Oferta
     */
    public function setSeoGenerate($seoGenerate) {
        $this->seo_generate = $seoGenerate;

        return $this;
    }

    /**
     * Get seo_generate
     *
     * @return boolean
     */
    public function getSeoGenerate() {
        return $this->seo_generate;
    }

    /**
     * Set seo_title
     *
     * @param string $seoTitle
     * @return Oferta
     */
    public function setSeoTitle($seoTitle) {
        $this->seo_title = $seoTitle;

        return $this;
    }

    /**
     * Get seo_title
     *
     * @return string
     */
    public function getSeoTitle() {
        return $this->seo_title;
    }

    /**
     * Set seo_keywords
     *
     * @param string $seoKeywords
     * @return Oferta
     */
    public function setSeoKeywords($seoKeywords) {
        $this->seo_keywords = $seoKeywords;

        return $this;
    }

    /**
     * Get seo_keywords
     *
     * @return string
     */
    public function getSeoKeywords() {
        return $this->seo_keywords;
    }

    /**
     * Set seo_description
     *
     * @param string $seoDescription
     * @return Oferta
     */
    public function setSeoDescription($seoDescription) {
        $this->seo_description = $seoDescription;

        return $this;
    }

    /**
     * Get seo_description
     *
     * @return string
     */
    public function getSeoDescription() {
        return $this->seo_description;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return Oferta
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Oferta
     */
    public function setCreatedAt($createdAt) {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Oferta
     */
    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    public function __toString() {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }
        return $this->getTitle();
    }
}
