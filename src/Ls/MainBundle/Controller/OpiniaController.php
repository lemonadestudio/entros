<?php

namespace Ls\MainBundle\Controller;

use Ls\MainBundle\Entity\Opinia;
use Ls\MainBundle\Form\OpiniaType;
use Ls\MainBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Opinia controller.
 *
 */
class OpiniaController extends Controller {

    /**
     * Lists all Opinia entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $limit = $em->getRepository('LsMainBundle:Setting')->findOneByLabel('limit_opinie')->getValue();
        $allow = 1;

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsMainBundle:Opinia', 'a')
            ->where('a.accepted = 1')
            ->orderBy('a.created_at', 'desc')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if (count($entities) < $limit) {
            $allow = 0;
        }

        $zespol = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:Zespol', 'a')
            ->getQuery()
            ->getResult();

        $today = new \DateTime();
        $news = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:News', 'a')
            ->where('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today->format('Y-m-d 00:00:00'))
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();

        foreach ($news as $key => $article) {
            $news[$key]->setContentShort(Tools::truncateWord($article->getContentShort(), 120, '...'));
        }

        return $this->render('LsMainBundle:Opinia:index.html.twig', array(
            'entities' => $entities,
            'zespol' => $zespol,
            'news' => $news,
            'allow' => $allow
        ));
    }

    public function formAction() {
        $entity = new Opinia();
        $form = $this->createForm(new OpiniaType(), $entity, array(
            'action' => $this->container->get('router')->generate('ls_main_opinia_send'),
            'method' => 'PUT',
        ));
        $form->add('submit', 'submit', array('label' => 'Wyślij'));

        return $this->render('LsMainBundle:Opinia:form.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function sendAction() {
        $em = $this->getDoctrine()->getManager();

        $entity = new Opinia();
        $form = $this->createForm(new OpiniaType(), $entity, array(
            'action' => $this->container->get('router')->generate('ls_main_opinia_send'),
            'method' => 'PUT',
        ));
        $form->add('submit', 'submit', array('label' => 'Wyślij'));

        $form->handleRequest($this->get('request'));
        $response = array(
            'result' => 'ERROR',
            'form' => '',
            'errors' => array()
        );

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $message_txt = '<h3>Dodana opinia:</h3>';
            $message_txt .= 'Imię i nazwisko: ' . $entity->getName() . '<br />';
            $message_txt .= 'Wiek: ' . $entity->getAge() . '<br />';
            $message_txt .= 'Miejscowość: ' . $entity->getCity() . '<br />';
            $message_txt .= '<h3>Treść:</h3>';
            $message_txt .= nl2br($entity->getContent());

            $main_email = $em->getRepository('LsMainBundle:Setting')->findOneByLabel('email_admin')->getValue();

            $emails = array();
            $emails[] = $main_email;

            $message = \Swift_Message::newInstance()
                ->setSubject('Dodanie nowej opinii pacjenta.')
                ->setFrom(array($this->container->getParameter('mailer_user') => 'New Life / Mailer'))
                ->setTo($emails)
                ->setBody($message_txt, 'text/html')
                ->addPart(strip_tags($message_txt), 'text/plain');

            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->container->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);

            $response['html'] = iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsMainBundle:Opinia:success.html.twig', array())->getContent());
            $response['result'] = 'OK';
        } else {
            $response['errors'] = Tools::getFormErrors($form);
        }

        return new JsonResponse($response);
    }

    public function ajaxMoreAction() {
        $em = $this->getDoctrine()->getManager();

        $page = $this->get('request')->request->get('page');
        $limit = $em->getRepository('LsMainBundle:Setting')->findOneByLabel('limit_opinie')->getValue();
        $start = $page * $limit;
        $allow = true;

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsMainBundle:Opinia', 'a')
            ->where('a.accepted = 1')
            ->orderBy('a.created_at', 'desc')
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if (count($entities) < $limit) {
            $allow = false;
        }

        $response = array(
            'allow' => $allow,
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsMainBundle:Opinia:list.html.twig', array(
                'entities' => $entities,
            ))->getContent())
        );

        return new JsonResponse($response);
    }
}
