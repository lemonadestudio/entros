<?php

namespace Ls\AdminBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType {
    private $container;

    public function __construct(ContainerInterface $serviceContainer) {
        $this->container = $serviceContainer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('username', null, array(
            'label' => 'Nazwa konta',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('email', null, array(
            'label' => 'Adres e-mail',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                )),
                new Email(array(
                    'message' => 'Niepoprawny adres e-mail'
                ))
            )
        ));
        $builder->add('plainPassword', 'text', array(
            'label' => 'Hasło',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('enabled', null, array(
            'label' => 'Aktywne'
        ));
        $builder->add('roles', 'choice', array(
            'choices' => $this->getExistingRoles(),
            'label' => 'Role',
            'expanded' => true,
            'multiple' => true,
            'mapped' => true,
        ));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $user = $event->getData();
            $form = $event->getForm();

            if (!$user || null === $user->getId()) {
                $form->add('plainPassword', 'text', array(
                    'label' => 'Hasło',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wypełnij pole'
                        ))
                    )
                ));
            } else {
                $form->add('plainPassword', 'text', array(
                    'label' => 'Hasło'
                ));
            }
        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\UserBundle\Entity\User',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_user';
    }

    public function getExistingRoles() {
        $roles = $this->container->getParameter('security.role_hierarchy.roles');

        $roles_choices = array();
        foreach ($roles as $role => $inherited_roles) {
            foreach ($inherited_roles as $id => $inherited_role) {
                if (!array_key_exists($inherited_role, $roles_choices)) {
                    $roles_choices[$inherited_role] = $inherited_role;
                }
            }

            if (!array_key_exists($role, $roles_choices)) {
                $roles_choices[$role] = $role . ' (' .
                    implode(', ', $inherited_roles) . ')';
            }
        }

        return $roles_choices;
    }
}
