<?php

namespace Ls\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class OpiniaType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', null, array(
            'label' => 'Twoje imię...',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('age', null, array(
            'label' => 'Wiek...'
        ));
        $builder->add('city', null, array(
            'label' => 'Miejscowość...'
        ));
        $builder->add('content', null, array(
            'label' => 'Treść Twojej referencji...',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\MainBundle\Entity\Opinia',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_opinia';
    }
}
