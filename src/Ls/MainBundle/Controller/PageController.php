<?php

namespace Ls\MainBundle\Controller;

use Ls\MainBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Page controller.
 *
 */
class PageController extends Controller {

    /**
     * Finds and displays a Page entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p', 'i')
            ->from('LsMainBundle:Page', 'p')
            ->leftJoin('p.icons', 'i')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $today = new \DateTime();
        $news = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:News', 'a')
            ->where('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today->format('Y-m-d 00:00:00'))
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();

        foreach ($news as $key => $article) {
            $news[$key]->setContentShort(Tools::truncateWord($article->getContentShort(), 120, '...'));
        }

        return $this->render('LsMainBundle:Page:show.html.twig', array(
            'entity' => $entity,
            'news' => $news
        ));
    }

}
