<?php

namespace Ls\MainBundle\Extensions;

use Ls\MainBundle\Utils\Config;

class SettingsExtension extends \Twig_Extension {

    protected $cms_config;

    function __construct(Config $config) {
        $this->cms_config = $config;
    }

    public function getGlobals() {
        return array(
            'cms_config' => $this->cms_config,
            'settings' => $this->cms_config->all(),
            'opinia_main' => $this->cms_config->opinia(),
            'icons_footer' => $this->cms_config->icons(),
        );
    }

    public function getName() {
        return 'cms_config';
    }

}