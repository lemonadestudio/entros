<?php

namespace Ls\AdminBundle\Controller;

use Ls\AdminBundle\Form\DifferenceType;
use Ls\MainBundle\Entity\Difference;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DifferenceController extends Controller {
    private $pager_limit_name = 'admin_page_pager_limit';

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $session = $this->container->get('session');

        $difference = $request->query->get('difference', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsMainBundle:Difference', 'e')
            ->getQuery();


        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $difference,
            $limit,
            array(
                'defaultSortFieldName' => 'e.arrangement',
                'defaultSortDirection' => 'asc',
            )
        );
        $entities->setTemplate('LsAdminBundle::paginator.html.twig');

        if ($difference > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_difference'));
        }
        // var_dump($query);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Różnice', $this->get('router')->generate('ls_admin_page'));

        return $this->render('LsAdminBundle:Difference:index.html.twig', array(
            'page' => $difference,
            'limit' => $limit,
            'entities' => $entities,
        ));
    }

    public function newAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $entity = new Difference();

        $form = $this->createForm(new DifferenceType(), $entity, array(
            'action' => $this->generateUrl('ls_admin_difference_new'),
            'method' => 'POST'
        ));
        $form->add('submit', 'submit', array('label' => 'Zapisz'));
        $form->add('submit_and_list', 'submit', array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', 'submit', array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie podstrony zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_difference_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_difference'));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_difference_new'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Różnice', $this->get('router')->generate('ls_admin_difference'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_difference_new'));

        return $this->render('LsAdminBundle:Difference:new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $entity = $em->getRepository('LsMainBundle:Difference')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Difference entity.');
        }

        $form = $this->createForm(new DifferenceType(), $entity, array(
            'action' => $this->generateUrl('ls_admin_difference_edit', array('id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', 'submit', array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', 'submit', array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja różnic zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_difference_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_difference'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Różnice', $this->get('router')->generate('ls_admin_difference'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_difference_edit', array('id' => $entity->getId())));

        return $this->render('LsAdminBundle:Difference:edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsMainBundle:Difference')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Difference entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie podstrony zakończone sukcesem.');

        return new Response('OK');
    }

    private function getMaxKolejnosc() {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsMainBundle:Difference', 'c')
            ->getQuery();

        $total = $query->getSingleScalarResult();
        return $total + 1;
    }

    public function batchAction() {
        $request = $this->get('request');

        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
            $breadcrumbs->addItem('Różnice', $this->get('router')->generate('ls_admin_difference'));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_difference_batch'));

            return $this->render('LsAdminBundle:Difference:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_page'));
        }
    }

    public function batchExecuteAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $batchSize = 50;
                    $index = 0;
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsMainBundle:Difference', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        if (($index % $batchSize) === 0) {
                            $em->flush();
                            $em->clear();
                        }
                        ++$index;
                    }
                    $em->flush();
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_difference'));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_difference'));
        }
    }

    public function setLimitAction() {
        $request = $this->get('request');
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
}
