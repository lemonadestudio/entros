<?php

namespace Ls\AdminBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\MainBundle\Entity\Gallery;
use Ls\MainBundle\Entity\GalleryPhoto;

class GalleryUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'postPersist',
            'preUpdate',
            'postUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Gallery) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
            if ($entity->getSeoGenerate()) {
                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoGenerate(false);
            }
        }

        if ($entity instanceof GalleryPhoto) {
            if (null === $entity->getArrangement()) {
                $gallery = $entity->getGallery();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsMainBundle:GalleryPhoto', 'c')
                    ->where('c.gallery = :gallery')
                    ->setParameter('gallery', $gallery)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Gallery) {
            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
                if (null !== $entity->getOldSlug()) {
                    $query = $em->createQueryBuilder()
                        ->select('c')
                        ->from('LsMainBundle:MenuItem', 'c')
                        ->where('c.route LIKE :route')
                        ->andWhere('c.route_parameters LIKE :slug')
                        ->setParameter('route', 'ls_main_gallery_show')
                        ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
                        ->getQuery();

                    $items = $query->getResult();

                    foreach ($items as $item) {
                        $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
                        $em->persist($item);
                    }
                    $em->flush();

                    $entity->setOldSlug($entity->getSlug());
                    $em->persist($entity);
                    $em->flush();
                } else {
                    $entity->setOldSlug($entity->getSlug());
                    $em->persist($entity);
                    $em->flush();
                }
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Gallery) {
            $entity->setUpdatedAt(new \DateTime());
            if ($entity->getSeoGenerate()) {
                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoGenerate(false);
            }
        }

        if ($entity instanceof GalleryPhoto) {
            if (null === $entity->getArrangement()) {
                $gallery = $entity->getGallery();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsMainBundle:GalleryPhoto', 'c')
                    ->where('c.gallery = :gallery')
                    ->setParameter('gallery', $gallery)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Gallery) {
            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsMainBundle:MenuItem', 'c')
                    ->where('c.route LIKE :route')
                    ->andWhere('c.route_parameters LIKE :slug')
                    ->setParameter('route', 'ls_main_gallery_show')
                    ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
                    ->getQuery();

                $items = $query->getResult();

                foreach ($items as $item) {
                    $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
                    $em->persist($item);
                }
                $em->flush();

                $entity->setOldSlug($entity->getSlug());
                $em->persist($entity);
                $em->flush();
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof GalleryPhoto) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();
                $gallery = $entity->getGallery();

                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsMainBundle:GalleryPhoto', 'c')
                    ->where('c.arrangement > :arrangement')
                    ->andWhere('c.gallery = :gallery')
                    ->setParameter('arrangement', $arrangement)
                    ->setParameter('gallery', $gallery)
                    ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['type'] = 'GalleryPhoto';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
            $entity->deletePhoto();
        }
    }
}