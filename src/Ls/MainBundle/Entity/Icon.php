<?php

namespace Ls\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Ls\MainBundle\Utils\Tools;

/**
 * Icon
 * @ORM\Table(name="icon")
 * @ORM\Entity
 */
class Icon {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Page",
     *     inversedBy="icons"
     * )
     * @ORM\JoinColumn(
     *     name="page_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @var \Ls\MainBundle\Entity\Page
     */
    private $page;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Product",
     *     inversedBy="icons"
     * )
     * @ORM\JoinColumn(
     *     name="product_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @var \Ls\MainBundle\Entity\Product
     */
    private $product;


    private $file;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Icon
     */
    public function setPhoto($photo) {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return Icon
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Set page
     *
     * @param \Ls\MainBundle\Entity\Page $page
     * @return Icon
     */
    public function setPage(Page $page = null) {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \Ls\MainBundle\Entity\Gallery
     */
    public function getPage() {
        return $this->page;
    }

    /**
     * Set product
     *
     * @param \Ls\MainBundle\Entity\Product $product
     * @return Icon
     */
    public function setProduct(Product $product = null) {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Ls\MainBundle\Entity\Gallery
     */
    public function getProduct() {
        return $this->product;
    }

    public function __toString() {
        if (is_null($this->getPhoto())) {
            return 'NULL';
        }
        return $this->getPhoto();
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFile(UploadedFile $file = null) {
        $this->deletePhoto();
        $this->file = $file;
        if (empty($this->photo)) {
            $this->setPhoto('empty');
        } else {
            $this->setPhoto('');
        }
    }

    public function getFile() {
        return $this->file;
    }

    public function deletePhoto() {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            if (file_exists($filename)) {
                @unlink($filename);
            }
        }
    }

    public function getPhotoWebPath() {
        if (null === $this->photo || empty($this->photo)) {
            return false;
        } else {
            return '/' . $this->getUploadDir() . '/' . $this->photo;
        }
    }

    public function getPhotoAbsolutePath() {
        if (null === $this->photo || empty($this->photo)) {
            return false;
        } else {
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
        }
    }

    public function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/icons';
    }

    public function upload($main = false) {
        if (null === $this->file) {
            return;
        }

        $ext = $this->file->guessExtension();

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;
        $thumb = \PhpThumbFactory::create($sSourceName);
        $dimensions = $thumb->getCurrentDimensions();

        //zmniejszenie zdjecia oryginalnego jesli jest za duze
        if ($main) {
            if ($dimensions['height'] > 98) {
                $thumb->resize(0, 98);
                $thumb->save($sSourceName);
                switch ($ext) {
                    case 'gif':
                        $sourceIm = imagecreatefromgif($sSourceName);
                        break;
                    case 'jpg':
                    case 'jpeg':
                        $sourceIm = imagecreatefromjpeg($sSourceName);
                        break;
                    case 'png':
                    default:
                        $sourceIm = imagecreatefrompng($sSourceName);
                        break;
                }

                $iw = imagesx($sourceIm);
                $ih = imagesy($sourceIm);

                $im = imagecreatetruecolor($iw, $ih);

                if (function_exists('imagecolorallocatealpha')) {
                    imagealphablending($im, false);
                    imagesavealpha($im, true);
                    $transparent = imagecolorallocatealpha($im, 255, 255, 255, 127);
                    imagefilledrectangle($im, 0, 0, $iw, $ih, $transparent);
                }
                imagecopyresampled($im, $sourceIm, 0, 0, 0, 0, $iw, $ih, $iw, $ih);
                // imagefilter($im, IMG_FILTER_BRIGHTNESS, 30);
                switch ($ext) {
                    case 'gif':
                        imagepng($im, $sSourceName);
                        break;
                    case 'jpg':
                    case 'jpeg':
                        imagejpeg($im, $sSourceName);
                        break;
                    case 'png':
                    default:
                        imagepng($im, $sSourceName);
                        break;
                }
            }
        } else {
            if ($dimensions['height'] > 98) {
                $thumb->resize(0, 100);
                $size = $thumb->getCurrentDimensions();
                $thumb->crop(0, 0, $size['width'], 98);
                $thumb->save($sSourceName);
            }
        }

        unset($this->file);
    }
}