<?php

namespace Ls\AdminBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class MenuItemType extends AbstractType {
    private $container;

    public function __construct(ContainerInterface $serviceContainer) {
        $this->container = $serviceContainer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('type', 'choice', array(
            'choices' => $this->getTypeChoices(),
            'label' => 'Typ linku'
        ));
        $builder->add('url', null, array(
            'label' => 'URL',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole',
                    'groups' => array(
                        'URL'
                    )
                ))
            )
        ));
        $builder->add('route', 'choice', array(
            'required' => false,
            'choices' => $this->getModuleChoices(),
            'placeholder' => '-- Wybierz --',
            'label' => 'Moduł',
            'constraints' => array(
                new NotNull(array(
                    'message' => 'Wybierz opcję',
                    'groups' => array(
                        'Modul'
                    )
                ))
            )
        ));
        $builder->add('route_parameters', 'hidden', array(
            'required' => false
        ));
        $builder->add('onclick', 'choice', array(
            'required' => false,
            'choices' => $this->getOnClickChoices(),
            'label' => 'Otwierany obiekt',
            'constraints' => array(
                new NotNull(array(
                    'message' => 'Wybierz opcję',
                    'groups' => array(
                        'Przycisk'
                    )
                ))
            )
        ));
        $builder->add('searchServices', 'hidden', array(
            'data' => $this->getSearchServicesJson(),
            'mapped' => false
        ), array(
            'style' => ''
        ));
        $builder->add('location', 'hidden', array());
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\MainBundle\Entity\MenuItem',
            'validation_groups' => function (FormInterface $form) {
                $type = $form->get('type')->getData();
                $groups = array('Default');
                switch ($type) {
                    case 'url':
                        $groups[] = 'URL';
                        break;

                    case 'route':
                        $groups[] = 'Modul';
                        break;

                    case 'button':
                        $groups[] = 'Przycisk';
                        break;

                    default:
                        break;
                }

                return $groups;
            },
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_menuitem';
    }

    public function getTypeChoices() {
        $types = array(
            'url' => 'Adres URL',
            'route' => 'Moduł',
                       'button' => 'Przycisk JavaScript'
        );

        return $types;
    }

    public function getLocationChoices() {
        $config_menu = $this->container->getParameter('ls_admin.menu');

        $locations = array();
        foreach ($config_menu['locations'] as $location) {
            $locations[$location['label']] = $location['name'];
        }

        return $locations;
    }

    public function getModuleChoices() {
        $config_menu = $this->container->getParameter('ls_admin.menu');

        $modules = array();
        foreach ($config_menu['modules'] as $module) {
            $modules[$module['route']] = $module['label'];
        }

        return $modules;
    }

    public function getOnClickChoices() {
        $config_menu = $this->container->getParameter('ls_admin.menu');

        $functions = array();
        foreach ($config_menu['onclick'] as $function) {
            $functions[$function['name']] = $function['label'];
        }

        return $functions;
    }

    public function getSearchServices() {
        $config_menu = $this->container->getParameter('ls_admin.menu');

        $services = array();
        foreach ($config_menu['modules'] as $module) {
            $services[$module['route']] = $module['get_elements_service'];
        }

        return $services;
    }

    public function getSearchServicesJson() {
        return json_encode($this->getSearchServices());
    }

}
