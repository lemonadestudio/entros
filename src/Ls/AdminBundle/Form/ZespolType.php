<?php

namespace Ls\AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;

class ZespolType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        // $builder->add('specjalizacja', 'entity', array(
        //     'label' => 'Specjalizacja',
        //     'class' => 'LsMainBundle:ZespolSpecjalizacja',
        // ));
        $builder->add('title', 'textarea', array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        // $builder->add('name', null, array(
        //     'label' => 'Imię i nazwisko',
        //     'constraints' => array(
        //         new NotBlank(array(
        //             'message' => 'Wypełnij pole'
        //         ))
        //     )
        // ));
        $builder->add('slug', null, array(
            'label' => 'Końcówka adresu URL'
        ));
        // $builder->add('position', null, array(
        //     'label' => 'Stanowisko',
        //     'constraints' => array(
        //         new NotBlank(array(
        //             'message' => 'Wypełnij pole'
        //         ))
        //     )
        // ));
        // $builder->add('content', null, array(
        //     'label' => 'Treść'
        // ));
        $builder->add('gallery', 'entity', array(
           'label' => 'Galeria',
           'attr' => array('class' => 'form-control'),
           'class' => 'LsMainBundle:Gallery',
           'required' => false,
           'query_builder' => function (EntityRepository $er) {
               return $er->createQueryBuilder('g')
                   ->where('g.on_main = 1');
           }
       ));
        $builder->add('seo_generate', null, array(
            'label' => 'Generuj automatycznie'
        ));
        $builder->add('seo_title', null, array(
            'label' => 'Tag "title"'
        ));
        $builder->add('seo_keywords', 'textarea', array(
            'label' => 'Meta "keywords"'
        ));
        $builder->add('seo_description', 'textarea', array(
            'label' => 'Meta "description"',
            'attr' => array(
                'rows' => 3
            )
        ));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $zespol = $event->getData();
            $form = $event->getForm();

            if (!$zespol || null === $zespol->getId()) {
                $form->add('file', 'file', array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wybierz zdjęcie'
                        )),
                        new Image(array(
                            'minWidth' => 288,
                            'minHeight' => 620,
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż 288px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 620px',
                        ))
                    )
                ));
            } else {
                $form->add('file', 'file', array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new Image(array(
                            'minWidth' => 288,
                            'minHeight' => 620,
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż 288px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 620px',
                        ))
                    )
                ));
            }
        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\MainBundle\Entity\Zespol',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_zespol';
    }
}
