<?php

namespace Ls\AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class DifferenceType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', 'textarea', array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('class', 'choice', array(
            'choices' => array(
                    '' => '',
                    'experience' => 'Doświadczenie',
                    'lore' => 'Wiedza',
                    'technology' => 'Technologia',
                    'professionalism' => 'Profesjonalizm',
                    'quality' => 'Jakość i terminowość'
                ),
            'label' => 'Klasa',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('content', 'textarea', array(
            'label' => 'Treść',
            'attr' => array(
                'class' => 'form-control'
                )
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\MainBundle\Entity\Difference',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_page';
    }
}
