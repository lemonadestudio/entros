<?php

namespace Ls\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class GalleryType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', 'textarea', array(
            'label' => 'Nazwa',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('on_list', null, array(
            'label' => 'Wyświetlaj na liście galerii'
        ));
        $builder->add('on_main', null, array(
            'label' => 'Realizacje'
        ));
        $builder->add('certyficate', null, array(
            'label' => 'Certyfikaty'
        ));
        $builder->add('attachable', null, array(
            'label' => 'Dołączalna do podstron i aktualności'
        ));
        $builder->add('seo_generate', null, array(
            'label' => 'Generuj automatycznie'
        ));
        $builder->add('seo_title', null, array(
            'label' => 'Tag "title"'
        ));
        $builder->add('seo_keywords', 'textarea', array(
            'label' => 'Meta "keywords"'
        ));
        $builder->add('seo_description', 'textarea', array(
            'label' => 'Meta "description"',
            'attr' => array(
                'rows' => 3
            )
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\MainBundle\Entity\Gallery',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_gallery';
    }
}
