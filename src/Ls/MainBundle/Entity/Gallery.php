<?php

namespace Ls\MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Gallery
 * @ORM\Table(name="gallery")
 * @ORM\Entity
 */
class Gallery
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $old_slug;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     * @var boolean
     */
    private $on_list;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     * @var boolean
     */
    private $on_main;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     * @var boolean
     */
    private $certyficate;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     * @var boolean
     */
    private $attachable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seo_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_description;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(
     *   targetEntity="News",
     *   mappedBy="gallery"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $news;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Page",
     *   mappedBy="gallery"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $pages;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Product",
     *   mappedBy="gallery"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $product;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Section",
     *   mappedBy="gallery"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $section;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Photo",
     *   mappedBy="gallery",
     *   cascade={"all"}
     * )
     * @ORM\OrderBy({"arrangement" = "ASC"})
     * @var \Doctrine\Common\Collections\Collection
     */
    private $photos;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->on_list = true;
        $this->on_main = false;
        $this->certyficate = false;
        $this->attachable = true;
        $this->seo_generate = true;
        $this->created_at = new \DateTime();
        $this->news = new ArrayCollection();
        $this->pages = new ArrayCollection();
        $this->product = new ArrayCollection();
        $this->section = new ArrayCollection();
        $this->photos = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Gallery
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Gallery
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set old_slug
     *
     * @param string $old_slug
     * @return News
     */
    public function setOldSlug($old_slug) {
        $this->old_slug = $old_slug;

        return $this;
    }

    /**
     * Get old_slug
     *
     * @return string
     */
    public function getOldSlug() {
        return $this->old_slug;
    }

    /**
     * Set on_list
     *
     * @param boolean $onList
     * @return Gallery
     */
    public function setOnList($onList)
    {
        $this->on_list = $onList;
    
        return $this;
    }

    /**
     * Get on_list
     *
     * @return boolean 
     */
    public function getOnList()
    {
        return $this->on_list;
    }

    /**
     * Set on_main
     *
     * @param boolean $onMain
     * @return Gallery
     */
    public function setOnMain($onMain)
    {
        $this->on_main = $onMain;
    
        return $this;
    }

    /**
     * Get on_main
     *
     * @return boolean 
     */
    public function getOnMain()
    {
        return $this->on_main;
    }

    /**
     * Set certyficate
     *
     * @param boolean $certyficate
     * @return Gallery
     */
    public function setCertyficate($certyficate)
    {
        $this->certyficate = $certyficate;
    
        return $this;
    }

    /**
     * Get certyficate
     *
     * @return boolean 
     */
    public function getCertyficate()
    {
        return $this->certyficate;
    }

    /**
     * Set attachable
     *
     * @param boolean $attachable
     * @return Gallery
     */
    public function setAttachable($attachable)
    {
        $this->attachable = $attachable;
    
        return $this;
    }

    /**
     * Get attachable
     *
     * @return boolean 
     */
    public function getAttachable()
    {
        return $this->attachable;
    }

    /**
     * Set seo_generate
     *
     * @param boolean $seoGenerate
     * @return Gallery
     */
    public function setSeoGenerate($seoGenerate) {
        $this->seo_generate = $seoGenerate;

        return $this;
    }

    /**
     * Get seo_generate
     *
     * @return boolean 
     */
    public function getSeoGenerate() {
        return $this->seo_generate;
    }

    /**
     * Set seo_title
     *
     * @param string $seoTitle
     * @return Gallery
     */
    public function setSeoTitle($seoTitle) {
        $this->seo_title = $seoTitle;

        return $this;
    }

    /**
     * Get seo_title
     *
     * @return string 
     */
    public function getSeoTitle() {
        return $this->seo_title;
    }

    /**
     * Set seo_keywords
     *
     * @param string $seoKeywords
     * @return Gallery
     */
    public function setSeoKeywords($seoKeywords) {
        $this->seo_keywords = $seoKeywords;

        return $this;
    }

    /**
     * Get seo_keywords
     *
     * @return string 
     */
    public function getSeoKeywords() {
        return $this->seo_keywords;
    }

    /**
     * Set seo_description
     *
     * @param string $seoDescription
     * @return Gallery
     */
    public function setSeoDescription($seoDescription) {
        $this->seo_description = $seoDescription;

        return $this;
    }

    /**
     * Get seo_description
     *
     * @return string 
     */
    public function getSeoDescription() {
        return $this->seo_description;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Gallery
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Gallery
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add news
     *
     * @param \Ls\MainBundle\Entity\News $news
     * @return Gallery
     */
    public function addNews(News $news)
    {
        $this->news[] = $news;
    
        return $this;
    }

    /**
     * Remove news
     *
     * @param \Ls\MainBundle\Entity\News $news
     */
    public function removeNews(News $news)
    {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Add pages
     *
     * @param \Ls\MainBundle\Entity\Page $pages
     * @return Gallery
     */
    public function addPage(Page $pages)
    {
        $this->pages[] = $pages;
    
        return $this;
    }

    /**
     * Remove pages
     *
     * @param \Ls\MainBundle\Entity\Page $pages
     */
    public function removePage(Page $pages)
    {
        $this->pages->removeElement($pages);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Add product
     *
     * @param \Ls\MainBundle\Entity\Product $product
     * @return Gallery
     */
    public function addProduct(Product $product)
    {
        $this->product[] = $product;
    
        return $this;
    }

    /**
     * Remove product
     *
     * @param \Ls\MainBundle\Entity\Product $product
     */
    public function removeProduct(Product $product)
    {
        $this->product->removeElement($product);
    }

    /**
     * Get product
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Add section
     *
     * @param \Ls\MainBundle\Entity\Section $section
     * @return Gallery
     */
    public function addSection(Section $section)
    {
        $this->section[] = $section;
    
        return $this;
    }

    /**
     * Remove section
     *
     * @param \Ls\MainBundle\Entity\Section $section
     */
    public function removeSection(Section $section)
    {
        $this->section->removeElement($section);
    }

    /**
     * Get section
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Add photos
     *
     * @param \Ls\MainBundle\Entity\GalleryPhoto $photos
     * @return Gallery
     */
    public function addPhoto(Photo $photos)
    {
        $this->photos[] = $photos;
    
        return $this;
    }

    /**
     * Remove photos
     *
     * @param \Ls\MainBundle\Entity\GalleryPhoto $photos
     */
    public function removePhoto(Photo $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhotos()
    {
        return $this->photos;
    }
    
    public function __toString() {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }
        return $this->getTitle();
    }
}