<?php

namespace Ls\MainBundle\Controller;

use Ls\MainBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Zespol controller.
 *
 */
class ZespolController extends Controller {

    /**
     * Lists all Zespol entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $zespol = $em->createQueryBuilder()
            ->select('a', 'z')
            ->from('LsMainBundle:ZespolSpecjalizacja', 'a')
            ->leftJoin('a.zespol', 'z')
            ->orderBy('a.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        $today = new \DateTime();
        $news = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:News', 'a')
            ->where('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today->format('Y-m-d 00:00:00'))
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();

        foreach ($news as $key => $article) {
            $news[$key]->setContentShort(Tools::truncateWord($article->getContentShort(), 120, '...'));
        }

        return $this->render('LsMainBundle:Zespol:index.html.twig', array(
            'zespol' => $zespol,
            'news' => $news
        ));
    }

    /**
     * Finds and displays a Zespol entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Zespol', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Zespol entity.');
        }

        $other = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Zespol', 'p')
            ->where('p.specjalizacja = :specjalizacja')
            ->andWhere('p.id != :id')
            ->setParameter('specjalizacja', $entity->getSpecjalizacja()->getId())
            ->setParameter('id', $entity->getId())
            ->getQuery()
            ->getResult();

        $today = new \DateTime();
        $news = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:News', 'a')
            ->where('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today->format('Y-m-d 00:00:00'))
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();

        foreach ($news as $key => $article) {
            $news[$key]->setContentShort(Tools::truncateWord($article->getContentShort(), 120, '...'));
        }

        return $this->render('LsMainBundle:Zespol:show.html.twig', array(
            'entity' => $entity,
            'other' => $other,
            'news' => $news
        ));
    }

}
