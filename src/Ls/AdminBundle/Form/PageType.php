<?php

namespace Ls\AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class PageType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        // $builder->add('slug', null, array(
        //     'label' => 'Końcówka adresu URL'
        // ));
//        $builder->add('gallery', 'entity', array(
//            'label' => 'Galeria',
//            'class' => 'LsMainBundle:Gallery',
//            'required' => false,
//            'query_builder' => function (EntityRepository $er) {
//                return $er->createQueryBuilder('g')
//                    ->where('g.attachable = 1');
//            }
//        ));
        $builder->add('content_short_generate', null, array(
            'label' => 'Automatycznie generuj krótką treść'
        ));
        // $builder->add('content_short', 'textarea', array(
        //     'label' => 'Krótka treść',
        //     'attr' => array(
        //         'rows' => 5
        //     )
        // ));
        $builder->add('content', 'textarea', array(
            'label' => 'Treść',
            'attr' => array(
                'class' => 'form-control'
                )
        ));
        // $builder->add('seo_generate', null, array(
        //     'label' => 'Generuj automatycznie'
        // ));
        // $builder->add('seo_title', null, array(
        //     'label' => 'Tag "title"'
        // ));
        // $builder->add('seo_keywords', 'textarea', array(
        //     'label' => 'Meta "keywords"'
        // ));
        // $builder->add('seo_description', 'textarea', array(
        //     'label' => 'Meta "description"',
        //     'attr' => array(
        //         'rows' => 3
        //     )
        // ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\MainBundle\Entity\Page',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_page';
    }
}
