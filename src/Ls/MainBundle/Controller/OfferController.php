<?php

namespace Ls\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Offer controller.
 *
 */
class OfferController extends Controller {

    /**
     * Lists all Offer entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $certyficates = $em->getRepository('LsMainBundle:Gallery')->findOneByCertyficate('1');

        $opinia = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:Opinia', 'a')
            ->where('a.accepted = 1')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();

        $offer = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:Oferta', 'a')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();

        $produkt = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:Product', 'a')
            ->orderBy('a.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('LsMainBundle:Offer:index.html.twig', array(
            'offer' => $offer,
            'opinia' => $opinia,
            'products' => $produkt,
            'certyficates' => $certyficates
        ));
    }

    /**
     * Finds and displays a Offer entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Product', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $opinia = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:Opinia', 'a')
            ->where('a.accepted = 1')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();

        $files = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:File', 'a')
            ->orderBy('a.arrangement', 'ASC')
            ->where('a.product = :id')
            ->setParameter('id', $entity->getId())
            ->getQuery()
            ->getResult();

        $products = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:Product', 'a')
            ->where('a.id != :id')
            ->setParameter('id', $entity->getId())
            ->orderBy('a.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        $sections = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:Section', 'a')
            ->orderBy('a.arrangement', 'ASC')
            ->where('a.product = :id')
            ->setParameter('id', $entity->getId())
            ->getQuery()
            ->getResult();

        if (null != $entity->getGallery()) {
            $product_pictures = $em->createQueryBuilder()
                ->select('a')
                ->from('LsMainBundle:Photo', 'a')
                ->orderBy('a.arrangement', 'ASC')
                ->where('a.gallery = :id')
                ->setParameter('id', $entity->getGallery()->getId())
                ->getQuery()
                ->getResult();
        } else {
            $product_pictures = null;
        }

        $sections_pictures = [];
        foreach ($sections as $section) {
            if (null != $section->getGallery()) {
                $section_pictures = $em->createQueryBuilder()
                    ->select('a')
                    ->from('LsMainBundle:Photo', 'a')
                    ->orderBy('a.arrangement', 'ASC')
                    ->where('a.gallery = :id')
                    ->setParameter('id', $section->getGallery()->getId())
                    ->getQuery()
                    ->getResult();

                $sections_pictures[$section->getId()] = $section_pictures;
            } else {
                $sections_pictures[$section->getId()] = null;
            }
        }

        return $this->render('LsMainBundle:Offer:show.html.twig', array(
            'product' => $entity,
            'products' => $products,
            'files' => $files,
            'sections' => $sections,
            'opinia' => $opinia,
            'produkt_pictures' => $product_pictures,
            'section_pictures' => $sections_pictures
        ));
    }
}
