<?php

namespace Ls\AdminBundle\Controller;

use Ls\AdminBundle\Form\MenuItemType;
use Ls\MainBundle\Entity\MenuItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class MenuController extends Controller {
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Menu', $this->get('router')->generate('ls_admin_menu'));

        $menu_cfg = $this->container->getParameter('ls_admin.menu');
        $locations = $menu_cfg['locations'];
        $menus = array();
        foreach ($locations as $location) {
            $menus[$location['label']]['name'] = $location['name'];
            $menus[$location['label']]['items'] = $em->createQueryBuilder()
                ->select('m', 'c'/*, 'cc'*/)
                ->from('LsMainBundle:MenuItem', 'm')
                ->leftJoin('m.children', 'c')
                // ->leftJoin('c.children', 'cc')
                ->where('m.location = :location')
                ->andWhere('m.parent is NULL')
                ->orderBy('m.arrangement', 'ASC')
                ->setParameter('location', $location['label'])
                ->getQuery()
                ->getResult();
        }

        return $this->render('LsAdminBundle:Menu:index.html.twig', array(
            'menus' => $menus,
        ));
    }

    public function newAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $entity = new MenuItem();

        $form = $this->createForm(new MenuItemType($this->container), $entity, array(
            'action' => $this->generateUrl('ls_admin_menu_new'),
            'method' => 'POST'
        ));
        $form->add('submit', 'submit', array('label' => 'Dodaj'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            if ($this->isXmlHttpRequest()) {
                return $this->renderJson(array(
                    'result' => 'ok',
                    'objectId' => $entity->getId()
                ));
            }

            return $this->redirect($this->generateUrl('ls_admin_menu_edit', array('id' => $entity->getId())));
        }

        return $this->render('LsAdminBundle:Menu:new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $entity = $em->getRepository('LsMainBundle:MenuItem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MenuItem entity.');
        }

        $form = $this->createForm(new MenuItemType($this->container), $entity, array(
            'action' => $this->generateUrl('ls_admin_menu_edit', array('id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', 'submit', array('label' => 'Zapisz'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            if ($this->isXmlHttpRequest()) {
                return $this->renderJson(array(
                    'result' => 'ok',
                    'objectId' => $entity->getId()
                ));
            }

            return $this->redirect($this->generateUrl('ls_admin_menu_edit', array('id' => $entity->getId())));
        }

        return $this->render('LsAdminBundle:Menu:edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        if ($this->isXmlHttpRequest()) {
            $entity = $em->getRepository('LsMainBundle:MenuItem')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MenuItem entity.');
            }

            $em->remove($entity);
            $em->flush();

            return $this->renderJson(array(
                'result' => 'ok',
                'objectId' => $id
            ));
        }
    }

    public function routeParametersAction() {
        $route = $this->container->get('request')->request->get('route');
        $parameters = $this->container->get('request')->request->get('parameters');
        $search = $this->container->get('request')->request->get('search');

        $services = $this->getSearchServices();

        $html = '';

        if (array_key_exists($route, $services)) {
            $service = $services[$route];
            if ($service != null) {
                if ($this->container->has($service)) {
                    $elements = $this->container->get($service)->search($search);

                    if (is_array($elements)) {
                        $html = $this->get('templating')->render('LsAdminBundle:Menu:routeparameters_choice.html.twig', array(
                            'parameters' => $parameters,
                            'elements' => $elements
                        ));
                    }
                }
            }
        }

        $response = new Response();
        $response->setContent($html);

        return $response;
    }

    private function updateChildren($elements, $parent_id, $arrangement) {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('LsMainBundle:MenuItem');
        $parent = $repository->find($parent_id);

        $arrangement++;
        foreach ($elements as $element) {
            $item_id = str_replace('list_', '', $element['id']);
            $item = $repository->find($item_id);
            if ($item) {
                $item->setArrangement($arrangement);
                $item->setParent($parent);
                if (isset($element['children'])) {
                    $arrangement = $this->updateChildren($element['children'], $item_id, $arrangement);
                } else {
                    $arrangement++;
                }
                $em->flush();
            }
        }

        return $arrangement;
    }

    public function saveOrderAction() {
        $elements = $this->container->get('request')->request->get('elements');
        $arrangement = 1;

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('LsMainBundle:MenuItem');

        if (is_array($elements)) {
            foreach ($elements as $element) {
                $item_id = str_replace('list_', '', $element['id']);
                $item = $repository->find($item_id);
                if ($item) {
                    $item->setArrangement($arrangement);
                    $item->setParent(null);
                    if (isset($element['children'])) {
                        $arrangement = $this->updateChildren($element['children'], $item_id, $arrangement);
                    } else {
                        $arrangement++;
                    }
                    $em->flush();
                }
            }
        }

        $response = new Response();
        $response->setContent('ok');

        return $response;
    }

    public function getSearchServices() {
        $config_menu = $this->container->getParameter('ls_admin.menu');

        $services = array();
        foreach ($config_menu['modules'] as $module) {
            $services[$module['route']] = $module['get_elements_service'];
        }

        return $services;
    }

    protected function renderJson($data, $status = 200, $headers = array()) {
        // fake content-type so browser does not show the download popup when this
        // response is rendered through an iframe (used by the jquery.form.js plugin)
        //  => don't know yet if it is the best solution
        if ($this->get('request')->get('_xml_http_request') && strpos($this->get('request')->headers->get('Content-Type'), 'multipart/form-data') === 0) {
            $headers['Content-Type'] = 'text/plain';
        } else {
            $headers['Content-Type'] = 'application/json';
        }

        return new Response(json_encode($data), $status, $headers);
    }

    protected function isXmlHttpRequest() {
        return $this->get('request')->isXmlHttpRequest() || $this->get('request')->get('_xml_http_request');
    }
}
