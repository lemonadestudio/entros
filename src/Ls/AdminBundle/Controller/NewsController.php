<?php

namespace Ls\AdminBundle\Controller;

use Ls\AdminBundle\Form\NewsType;
use Ls\MainBundle\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class NewsController extends Controller {
    private $pager_limit_name = 'admin_news_pager_limit';

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e', 'g')
            ->from('LsMainBundle:News', 'e')
            ->leftJoin('e.gallery', 'g')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.published_at',
                'defaultSortDirection' => 'desc',
            )
        );
        $entities->setTemplate('LsAdminBundle::paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_news'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Aktualności', $this->get('router')->generate('ls_admin_news'));

        return $this->render('LsAdminBundle:News:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
        ));
    }

    public function newAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $entity = new News();
        $size = $entity->getThumbSize('detail');

        $form = $this->createForm(new NewsType(), $entity, array(
            'action' => $this->generateUrl('ls_admin_news_new'),
            'method' => 'POST'
        ));
        $form->add('submit', 'submit', array('label' => 'Zapisz'));
        $form->add('submit_and_list', 'submit', array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', 'submit', array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            if (null !== $entity->getFile()) {
                $sFileName = uniqid('news-image-') . '.' . $entity->getFile()->guessExtension();
                $entity->setPhoto($sFileName);
                $entity->upload();
            }
            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls.admin.sitemap');
            $sitemap->generate();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie aktualności zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_news_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_news'));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_news_new'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Aktualności', $this->get('router')->generate('ls_admin_news'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_news_new'));

        return $this->render('LsAdminBundle:News:new.html.twig', array(
            'form' => $form->createView(),
            'size' => $size
        ));
    }

    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $entity = $em->getRepository('LsMainBundle:News')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find News entity.');
        }
        $size = $entity->getThumbSize('detail');

        $form = $this->createForm(new NewsType(), $entity, array(
            'action' => $this->generateUrl('ls_admin_news_edit', array('id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', 'submit', array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', 'submit', array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            if (null !== $entity->getFile()) {
                $sFileName = uniqid('news-image-') . '.' . $entity->getFile()->guessExtension();
                $entity->setPhoto($sFileName);
                $entity->upload();
            }
            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls.admin.sitemap');
            $sitemap->generate();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja aktualności zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_news_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_news'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Aktualności', $this->get('router')->generate('ls_admin_news'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_news_edit', array('id' => $entity->getId())));

        return $this->render('LsAdminBundle:News:edit.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
            'size' => $size
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsMainBundle:News')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find News entity.');
        }

        $em->remove($entity);
        $em->flush();

        $sitemap = $this->get('ls.admin.sitemap');
        $sitemap->generate();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie aktualności zakończone sukcesem.');

        return new Response('OK');
    }

    public function kadrujAction($id) {
        $em = $this->getDoctrine()->getManager();
        $type = $this->get('request')->get('type');

        $entity = $em->getRepository('LsMainBundle:News')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find News entity.');
        }

        if (null === $entity->getPhotoAbsolutePath()) {
            return $this->redirect($this->generateUrl('ls_admin_news'));
        } else {
            $size = $entity->getThumbSize($type);
            $photo = $entity->getPhotoSize();
            $thumb_ratio = $size['width'] / $size['height'];
            $photo_ratio = $photo['width'] / $photo['height'];

            $thumb_conf = array();
            $thumb_conf['photo_width'] = $photo['width'];
            $thumb_conf['photo_height'] = $photo['height'];
            if ($thumb_ratio < $photo_ratio) {
                $thumb_conf['width'] = round($photo['height'] * $thumb_ratio);
                $thumb_conf['height'] = $photo['height'];
                $thumb_conf['x'] = ceil(($photo['width'] - $thumb_conf['width']) / 2);
                $thumb_conf['y'] = 0;
            } else {
                $thumb_conf['width'] = $photo['width'];
                $thumb_conf['height'] = round($photo['width'] / $thumb_ratio);
                $thumb_conf['x'] = 0;
                $thumb_conf['y'] = ceil(($photo['height'] - $thumb_conf['height']) / 2);
            }

            $preview = array();
            $preview['width'] = 150;
            $preview['height'] = round(150 / $thumb_ratio);

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
            $breadcrumbs->addItem('Aktualności', $this->get('router')->generate('ls_admin_news'));
            $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_news_edit', array('id' => $entity->getId())));
            $breadcrumbs->addItem('Kadrowanie', $this->get('router')->generate('ls_admin_news_crop', array('id' => $entity->getId(), 'type' => $type)));

            return $this->render('LsAdminBundle:News:kadruj.html.twig', array(
                'entity' => $entity,
                'preview' => $preview,
                'thumb_conf' => $thumb_conf,
                'size' => $size,
                'aspect' => $thumb_ratio,
                'type' => $type,
            ));
        }
    }

    public function kadrujZapiszAction($id) {
        $em = $this->getDoctrine()->getManager();
        $type = $this->get('request')->get('type');
        $x = $this->get('request')->get('x');
        $y = $this->get('request')->get('y');
        $x2 = $this->get('request')->get('x2');
        $y2 = $this->get('request')->get('y2');

        $entity = $em->getRepository('LsMainBundle:News')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find News entity.');
        }

        $entity->Thumb($x, $y, $x2, $y2, $type);

        $this->get('session')->getFlashBag()->add('success', 'Kadrowanie miniatury zakończone sukcesem.');

        return $this->redirect($this->generateUrl('ls_admin_news_edit', array('id' => $entity->getId())));
    }

    public function batchAction() {
        $request = $this->get('request');

        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
            $breadcrumbs->addItem('Aktualności', $this->get('router')->generate('ls_admin_news'));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_news_batch'));

            return $this->render('LsAdminBundle:News:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_news'));
        }
    }

    public function batchExecuteAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $batchSize = 50;
                    $index = 0;
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsMainBundle:News', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        if (($index % $batchSize) === 0) {
                            $em->flush();
                            $em->clear();
                        }
                        ++$index;
                    }
                    $em->flush();

                    $sitemap = $this->get('ls.admin.sitemap');
                    $sitemap->generate();

                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_news'));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_news'));
        }
    }

    public function setLimitAction() {
        $request = $this->get('request');
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
}
