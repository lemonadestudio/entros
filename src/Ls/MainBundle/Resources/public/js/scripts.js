if (!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)) {
    var s = skrollr.init({
        forceHeight: false
    });
}

if (window.location.hash !== '/' && window.location.hash !== '' && window.location.hash !== '#/') {
    offset = $(window.location.hash).offset().top;
    $('[href=' + window.location.hash + ']').addClass('active');
}


function initMap(latitude, longitude) {
    if ($('#map_canvas').get(0)) {
        var latLng = new google.maps.LatLng(latitude, longitude);
        var homeLatLng = new google.maps.LatLng(latitude, longitude);

        var map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 12,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker = new MarkerWithLabel({
            position: homeLatLng,
            draggable: false,
            map: map,
            labelContent: "Entros",
            labelAnchor: new google.maps.Point(4, 8),
            labelClass: "labels", // the CSS class for the label
            labelStyle: {opacity: 1}
        });

        var iw = new google.maps.InfoWindow({
            content: "Entros sp. z o.o."
        });
        google.maps.event.addListener(marker, "click", function (e) {
            iw.open(map, marker);
        });
    }
}

var $root = $('html, body');
$('.main').find('a#logo, #menu_top > li > a, #menu_top > li > div > a, #menu_bottom a, a.meet_us').on('click', function (e) {

    e.preventDefault();

    var href = $.attr(this, 'href');
    var offset = 0;

    $('.main').find('#menu_top a').removeClass('active');
    $('.main').find('#menu_bottom a').removeClass('active');

    if (href !== '/' && href !== '') {
        offset = $(href).offset().top;
        $('[href=' + href + ']').addClass('active');
    }

    if ($('.main #show_menu').hasClass('active')) {
        $('.main #show_menu').removeClass('active');
        $('.main #menu_top').hide();
    }

    $root.animate({
        scrollTop: offset
    }, 500, function () {
        if (href !== '/' && href !== '') {
            window.location.hash = href;
        } else {
            window.location.hash = '';
        }
    });
    return false;
});

$('.element .elementShow').on('click', function (e) {
    e.preventDefault();

    var that = this;
    var parent = $(this).parents('.element');

    parent.find('.to-hide').stop(true, true).slideDown("slow", function () {
        $(that).hide();
        parent.find('.elementHide').show();
    });
});

$('.element .elementHide').on('click', function (e) {
    e.preventDefault();

    var that = this;
    var parent = $(this).parents('.element');

    parent.find('.to-hide').stop(true, true).slideUp("slow", function () {
        $(that).hide();
        parent.find('.elementShow').show();
    });
});

$('.realizations').find('.details').on('click', function (e) {
    e.preventDefault();

    var parent = $(this).parents().parents();

    var urls = [];

    parent.find('.photo').each(function () {
        var url = {
            href: $(this).data('url')
        };
        urls.push(url);
    });

    $.fancybox.open(urls, {
        fitToView: true,
        openEffect: 'elastic',
        closeEffect: 'elastic',
        maxWidth: "70%",
        scrolling: "no",
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
});

window.onresize = resize;
resize();

function resize() {
    if ($('.main #show_menu').is(':hidden')) {
        $('#differences .differences_container > div').removeClass('active');
        $('.main #show_menu').removeClass('active');
        $('.main #menu_top').height('auto');
        $('.main #menu_top').show();
    } else {
        $('.main #menu_top').hide();
    }

    if ($('#back_small').is(':hidden')) {
        if ($('#base .container .content .right .download').length > 0) {
            $('#base .container .content .left').prepend($('#base .container .content .right .download'));
            $('#base .container .content .right .download').remove();
        }
    } else {
        if ($('#base .container .content .left .download').length > 0) {
            $('#base .container .content .right').prepend($('#base .container .content .left .download'));
            $('#base .container .content .left .download').remove();
        }
    }
}

$('#differences .differences_container > div').on('click', function (e) {
    e.preventDefault();

    if ($('.main #show_menu').not(':hidden')) {
        var that = this;

        if (!$(that).hasClass('active')) {
            $('#differences .differences_container > div').removeClass('active');
            $(that).addClass('active');
        } else {
            $('#differences .differences_container > div').removeClass('active');
        }
    }
});

$('#show_menu').on('click', function () {
    if ($('.main #show_menu').hasClass('active')) {
        $('.main #show_menu').removeClass('active');
        $('.main #menu_top').slideUp("slow");
    } else {
        $('.main #show_menu').addClass('active');
        $('.main #menu_top').slideDown("slow");
    }
});