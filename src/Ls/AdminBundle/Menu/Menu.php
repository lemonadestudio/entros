<?php

namespace Ls\AdminBundle\Menu;

use Symfony\Component\DependencyInjection\ContainerAware;
use Knp\Menu\FactoryInterface;

class Menu extends ContainerAware {

    public function managedMenu(FactoryInterface $factory) {
        $router = $this->container->get('router');
        $request = $this->container->get('request');

        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        switch ($request->get('_route')) {
            case 'ls_admin_oferta':
            case 'ls_admin_oferta_new':
            case 'ls_admin_oferta_edit':
            case 'ls_admin_oferta_batch':
                $menu->setCurrentUri($router->generate('ls_admin_oferta'));
                break;
            case 'ls_admin_icon':
            case 'ls_admin_icon_new':
            case 'ls_admin_icon_edit':
            case 'ls_admin_icon_batch':
                $menu->setCurrentUri($router->generate('ls_admin_icon'));
                break;
            case 'ls_admin_haslo':
            case 'ls_admin_haslo_new':
            case 'ls_admin_haslo_edit':
            case 'ls_admin_haslo_batch':
                $menu->setCurrentUri($router->generate('ls_admin_haslo'));
                break;
            case 'ls_admin_menu':
                $menu->setCurrentUri($router->generate('ls_admin_menu'));
                break;
            case 'ls_admin_news':
            case 'ls_admin_news_new':
            case 'ls_admin_news_edit':
            case 'ls_admin_news_batch':
                $menu->setCurrentUri($router->generate('ls_admin_news'));
                break;
            case 'ls_admin_opinia':
            case 'ls_admin_opinia_batch':
                $menu->setCurrentUri($router->generate('ls_admin_opinia'));
                break;
            case 'ls_admin_page':
            case 'ls_admin_page_new':
            case 'ls_admin_page_edit':
            case 'ls_admin_page_batch':
            case 'ls_admin_page_icon':
            case 'ls_admin_page_icon_new':
            case 'ls_admin_page_icon_edit':
            case 'ls_admin_page_icon_batch':
                $menu->setCurrentUri($router->generate('ls_admin_page'));
                break;
            case 'ls_admin_setting':
            case 'ls_admin_setting_new':
            case 'ls_admin_setting_edit':
            case 'ls_admin_setting_batch':
                $menu->setCurrentUri($router->generate('ls_admin_setting'));
                break;
            case 'ls_admin_user':
            case 'ls_admin_user_new':
            case 'ls_admin_user_edit':
            case 'ls_admin_user_batch':
                $menu->setCurrentUri($router->generate('ls_admin_user'));
                break;
            case 'ls_admin_zespol':
            case 'ls_admin_zespol_new':
            case 'ls_admin_zespol_edit':
            case 'ls_admin_zespol_batch':
                $menu->setCurrentUri($router->generate('ls_admin_zespol'));
                break;
            case 'ls_admin_zespol_specjalizacja':
            case 'ls_admin_zespol_specjalizacja_new':
            case 'ls_admin_zespol_specjalizacja_edit':
            case 'ls_admin_zespol_specjalizacja_batch':
                $menu->setCurrentUri($router->generate('ls_admin_zespol_specjalizacja'));
                break;
            case 'ls_admin_product':
            case 'ls_admin_product_new':
            case 'ls_admin_product_edit':
            case 'ls_admin_product_batch':
            case 'ls_admin_product_icon':
            case 'ls_admin_product_icon_new':
            case 'ls_admin_product_icon_edit':
            case 'ls_admin_product_icon_batch':
                $menu->setCurrentUri($router->generate('ls_admin_product'));
                break;
            case 'ls_admin_difference':
            case 'ls_admin_difference_new':
            case 'ls_admin_difference_edit':
            case 'ls_admin_difference_batch':
                $menu->setCurrentUri($router->generate('ls_admin_difference'));
            default:
                $menu->setCurrentUri($this->container->get('request')->getRequestUri());
                break;
        }

        $content = $menu->addChild('Zarządzanie treścią', array(
            'uri' => '#',
            'linkAttributes' => array(
                'class' => 'dropdown-toggle',
                'data-toggle' => 'dropdown'
            )
        ));
        $content->setAttribute('class', 'dropdown');
        $content->addChild('Menu', array(
            'route' => 'ls_admin_menu',
        ));
        $content->addChild('Segmenty na głównej', array(
            'route' => 'ls_admin_page',
        ));
        $content->addChild('Oferta', array(
            'route' => 'ls_admin_oferta',
        ));
        $content->addChild('Realizacje', array(
            'route' => 'ls_admin_zespol',
        ));
        $content->addChild('Produkty', array(
            'route' => 'ls_admin_product',
        ));
        $content->addChild('Co nas wyróżnia', array(
            'route' => 'ls_admin_difference',
        ));
        $content->addChild('Aktualności', array(
            'route' => 'ls_admin_news',
        ));
        $content->addChild('Logotypy', array(
            'route' => 'ls_admin_icon',
        ));
        $content->addChild('Opinie', array(
            'route' => 'ls_admin_opinia',
        ));

        $media = $menu->addChild('Multimedia', array(
            'uri' => '#',
            'linkAttributes' => array(
                'class' => 'dropdown-toggle',
                'data-toggle' => 'dropdown'
            )
        ));
        $media->addChild('Galerie', array(
            'route' => 'ls_admin_gallery',
        ));

        $other = $menu->addChild('Inne', array(
            'uri' => '#',
            'linkAttributes' => array(
                'class' => 'dropdown-toggle',
                'data-toggle' => 'dropdown'
            )
        ));
        $other->setAttribute('class', 'dropdown');
        $other->addChild('Ustawienia', array(
            'route' => 'ls_admin_setting',
        ));
        // $other->addChild('Specjalizacje', array(
        //     'route' => 'ls_admin_zespol_specjalizacja',
        // ));

        $user = $menu->addChild('Użytkownicy', array(
            'uri' => '#',
            'linkAttributes' => array(
                'class' => 'dropdown-toggle',
                'data-toggle' => 'dropdown'
            )
        ));
        $user->setAttribute('class', 'dropdown');
        $user->addChild('Użytkownicy', array(
            'route' => 'ls_admin_user',
        ));

        return $menu;
    }
}

