<?php

namespace Ls\AdminBundle\Controller;

use Ls\AdminBundle\Form\PhotoType;
use Ls\MainBundle\Entity\Photo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class GalleryPhotoController extends Controller {
    private $pager_limit_name = 'admin_page_icon_pager_limit';

    public function indexAction($pageId) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $session = $this->container->get('session');

        $pageEntity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Gallery', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $pageId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $pageEntity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsMainBundle:Photo', 'e')
            ->where('e.gallery = :page')
            ->setParameter('page', $pageId)
            ->orderBy('e.arrangement', 'asc')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit
        );
        $entities->setTemplate('LsAdminBundle::paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_page_icon'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Galerie', $this->get('router')->generate('ls_admin_gallery'));
        $breadcrumbs->addItem($pageEntity->__toString(), $this->get('router')->generate('ls_admin_gallery_edit', array('id' => $pageEntity->getId())));
        $breadcrumbs->addItem('Zdjęcia', $this->get('router')->generate('ls_admin_gallery_photo', array('pageId' => $pageId)));

        return $this->render('LsAdminBundle:GalleryPhoto:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'page_id' => $pageEntity->getId(),
            'entities' => $entities,
        ));
    }

    public function addManyAction(Request $request, $galleryId) {
        $em = $this->getDoctrine()->getManager();


        $pageEntity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Gallery', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $pageEntity) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }
        $files = $request->request->get('files');
        $counter = 0;
        foreach($files as $file) {
            $entity = new Photo;
            $entity->setGallery($pageEntity);

            $entity->setPhoto($file);
            $pName = $entity->getPhoto();
            $extension = explode('.',$entity->getPhoto());
            $extension1 = array_reverse($extension);
            $sFileName = uniqid('gallery-photo-') . '.'.$extension1[0];
            $pDir = $entity->getTempDir();
            $sDir = $entity->getUploadRootDir();
            rename("$pDir/$pName" , "$sDir/$sFileName");
            $entity->setPhoto($sFileName);
            $em->persist($entity);
            $em->flush();
            $entity->createThumb();
            $counter ++;
        }     
        $this->get('session')->getFlashBag()->add('success', 'Dodawanie '. $counter .' plików zakończone sukcesem.');
        return new Response('OK');
    }

    public function newAction($pageId) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $pageEntity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Gallery', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $pageId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $pageEntity) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $entity = new Photo();
        $entity->setGallery($pageEntity);

        $form = $this->createForm(new PhotoType(), $entity, array(
            'action' => $this->generateUrl('ls_admin_gallery_photo_new', array('pageId' => $pageId)),
            'method' => 'POST'
        ));
        $form->add('submit', 'submit', array('label' => 'Zapisz'));
        $form->add('submit_and_list', 'submit', array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', 'submit', array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            if (null !== $entity->getFile()) {
                $sFileName = uniqid('gallery-photo-') . '.' . $entity->getFile()->guessExtension();
                $entity->setPhoto($sFileName);
                $entity->upload();
            }
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie logotypu zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_photo_edit', array('pageId' => $pageId, 'id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_photo', array('pageId' => $pageId)));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_photo_new', array('pageId' => $pageId)));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Galerie', $this->get('router')->generate('ls_admin_gallery'));
        $breadcrumbs->addItem($pageEntity->__toString(), $this->get('router')->generate('ls_admin_gallery_edit', array('id' => $pageEntity->getId())));
        $breadcrumbs->addItem('Zdjęcia', $this->get('router')->generate('ls_admin_gallery_photo', array('pageId' => $pageId)));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_gallery_photo_new', array('pageId' => $pageId)));

        return $this->render('LsAdminBundle:GalleryPhoto:new.html.twig', array(
            'form' => $form->createView(),
            'page_id' => $pageId
        ));
    }

    public function editAction($pageId, $id) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $pageEntity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Gallery', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $pageId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $pageEntity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $entity = $em->getRepository('LsMainBundle:Photo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Photo entity.');
        }

        $form = $this->createForm(new PhotoType(), $entity, array(
            'action' => $this->generateUrl('ls_admin_gallery_photo_edit', array('pageId' => $pageId, 'id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', 'submit', array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', 'submit', array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            if (null !== $entity->getFile()) {
                $sFileName = uniqid('gallery-photo-') . '.' . $entity->getFile()->guessExtension();
                $entity->setPhoto($sFileName);
                $entity->upload();
            }
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja logotypu zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_photo_edit', array('pageId' => $pageId, 'id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_photo', array('pageId' => $pageId)));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Galerie', $this->get('router')->generate('ls_admin_gallery'));
        $breadcrumbs->addItem($pageEntity->__toString(), $this->get('router')->generate('ls_admin_gallery_edit', array('id' => $pageEntity->getId())));
        $breadcrumbs->addItem('Zdjęcia', $this->get('router')->generate('ls_admin_gallery_photo', array('pageId' => $pageId)));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_gallery_photo_edit', array('pageId' => $pageId, 'id' => $entity->getId())));

        return $this->render('LsAdminBundle:GalleryPhoto:edit.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
            'page_id' => $pageId
        ));
    }

    public function deleteAction($pageId, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsMainBundle:Photo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Photo entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie zdjęcia zakończone sukcesem.');

        return new Response('OK');
    }

    public function batchAction($pageId) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $pageEntity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Gallery', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $pageId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $pageEntity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
            $breadcrumbs->addItem('Galerie', $this->get('router')->generate('ls_admin_gallery'));
            $breadcrumbs->addItem($pageEntity->__toString(), $this->get('router')->generate('ls_admin_gallery_edit', array('id' => $pageEntity->getId())));
            $breadcrumbs->addItem('Zdjęcia', $this->get('router')->generate('ls_admin_gallery_photo', array('pageId' => $pageId)));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_gallery_photo_batch', array('pageId' => $pageId)));

            return $this->render('LsAdminBundle:GalleryPhoto:batch.html.twig', array(
                'page_id' => $pageId,
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_gallery_photo', array('pageId' => $pageId)));
        }
    }

    public function batchExecuteAction($pageId) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $batchSize = 50;
                    $index = 0;
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsMainBundle:Photo', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        if (($index % $batchSize) === 0) {
                            $em->flush();
                            $em->clear();
                        }
                        ++$index;
                    }
                    $em->flush();
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_gallery_photo', array('pageId' => $pageId)));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_gallery_photo', array('pageId' => $pageId)));
        }
    }

    public function setLimitAction($pageId) {
        $request = $this->get('request');
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }

    public function kadrujAction($id, $pageId) {
        $em = $this->getDoctrine()->getManager();
        $type = $this->get('request')->get('type');

        $entity = $em->getRepository('LsMainBundle:Photo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Photo entity.');
        }

        if (null === $entity->getPhotoAbsolutePath()) {
            return $this->redirect($this->generateUrl('ls_admin_gallery_photo'));
        } else {
            $size = $entity->getThumbSize($type);
            $photo = $entity->getPhotoSize();
            $thumb_ratio = $size['width'] / $size['height'];
            $photo_ratio = $photo['width'] / $photo['height'];

            $thumb_conf = array();
            $thumb_conf['photo_width'] = $photo['width'];
            $thumb_conf['photo_height'] = $photo['height'];
            if ($thumb_ratio < $photo_ratio) {
                $thumb_conf['width'] = round($photo['height'] * $thumb_ratio);
                $thumb_conf['height'] = $photo['height'];
                $thumb_conf['x'] = ceil(($photo['width'] - $thumb_conf['width']) / 2);
                $thumb_conf['y'] = 0;
            } else {
                $thumb_conf['width'] = $photo['width'];
                $thumb_conf['height'] = round($photo['width'] / $thumb_ratio);
                $thumb_conf['x'] = 0;
                $thumb_conf['y'] = ceil(($photo['height'] - $thumb_conf['height']) / 2);
            }

            $preview = array();
            $preview['width'] = 150;
            $preview['height'] = round(150 / $thumb_ratio);

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
            $breadcrumbs->addItem('Zdjęcia', $this->get('router')->generate('ls_admin_gallery_photo', array('pageId' => $pageId)));
            $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_gallery_photo_edit', array('id' => $entity->getId(), 'pageId' => $pageId)));
            $breadcrumbs->addItem('Kadrowanie', $this->get('router')->generate('ls_admin_gallery_photo_crop', array('id' => $entity->getId(), 'type' => $type, 'pageId' => $pageId)));
        // die('boom');

            return $this->render('LsAdminBundle:GalleryPhoto:kadruj.html.twig', array(
                'entity' => $entity,
                'preview' => $preview,
                'thumb_conf' => $thumb_conf,
                'size' => $size,
                'aspect' => $thumb_ratio,
                'type' => $type,
                'pageId' => $pageId
            ));
        }
    }

    public function kadrujZapiszAction($id, $pageId) {
        $em = $this->getDoctrine()->getManager();
        $type = $this->get('request')->get('type');
        $x = $this->get('request')->get('x');
        $y = $this->get('request')->get('y');
        $x2 = $this->get('request')->get('x2');
        $y2 = $this->get('request')->get('y2');

        $entity = $em->getRepository('LsMainBundle:Photo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Zespol entity.');
        }

        $entity->Thumb($x, $y, $x2, $y2, $type);

        $this->get('session')->getFlashBag()->add('success', 'Kadrowanie miniatury zakończone sukcesem.');

        return $this->redirect($this->generateUrl('ls_admin_gallery_photo_edit', array('id' => $entity->getId(), 'pageId' => $pageId)));
    }

    private function getMaxKolejnosc($gallery_id) {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            // ->select('c')
            ->from('LsMainBundle:Photo', 'c')
            ->where('c.gallery = :id')
            ->setParameter('id', $gallery_id)
            ->getQuery();

        $total = $query->getSingleScalarResult();
        // $total = $query->getResult();
        // var_dump($total);
        return $total + 1;
    }

    public function moveDownAction($id) {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsMainBundle:Photo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Photo entity.');
        }

        $max = $this->getMaxKolejnosc($entity->getGallery()->getId());
        $old_kolejnosc = $entity->getArrangement();
        $new_kolejnosc = $old_kolejnosc + 1;
        if ($new_kolejnosc < $max) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsMainBundle:Photo', 'c')
                ->where('c.arrangement = :arrangement')
                ->andWhere('c.gallery = :gallery')
                ->setParameter('arrangement', $new_kolejnosc)
                ->setParameter('gallery', $entity->getGallery()->getId())
                ->getQuery();

            $old_entity_id = $query->getSingleScalarResult();
            $old_entity = $em->getRepository('LsMainBundle:Photo')->findOneById($old_entity_id);
            $old_entity->setArrangement(0);
            $em->persist($old_entity);
            $em->flush();
            $entity->setArrangement($new_kolejnosc);
            $em->persist($entity);
            $em->flush();
            $old_entity->setArrangement($old_kolejnosc);
            $em->persist($old_entity);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->add('success', 'Przeniesienie podstrony do dołu zakończone sukcesem.');

        return new Response('OK');
    }

    public function moveUpAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsMainBundle:Photo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $old_kolejnosc = $entity->getArrangement();
        $new_kolejnosc = $old_kolejnosc - 1;
        if ($new_kolejnosc > 0) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsMainBundle:Photo', 'c')
                ->where('c.arrangement = :arrangement')
                ->andWhere('c.gallery = :gallery')
                ->setParameter('arrangement', $new_kolejnosc)
                ->setParameter('gallery', $entity->getGallery()->getId())
                ->getQuery();

            $old_entity_id = $query->getSingleScalarResult();
            $old_entity = $em->getRepository('LsMainBundle:Photo')->findOneById($old_entity_id);
            $old_entity->setArrangement(0);
            $em->persist($old_entity);
            $em->flush();
            $entity->setArrangement($new_kolejnosc);
            $em->persist($entity);
            $em->flush();
            $old_entity->setArrangement($old_kolejnosc);
            $em->persist($old_entity);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->add('success', 'Przeniesienie podstrony do góry zakończone sukcesem.');

        return new Response('OK');
    }
}
