<?php

namespace Ls\MainBundle\Controller;

use Ls\MainBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Badanie controller.
 *
 */
class BadanieController extends Controller {

    /**
     * Lists all Badanie entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $badania = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:Badanie', 'a')
            ->orderBy('a.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        $today = new \DateTime();
        $news = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:News', 'a')
            ->where('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today->format('Y-m-d 00:00:00'))
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();

        foreach ($news as $key => $article) {
            $news[$key]->setContentShort(Tools::truncateWord($article->getContentShort(), 120, '...'));
        }

        return $this->render('LsMainBundle:Badanie:index.html.twig', array(
            'badania' => $badania,
            'news' => $news
        ));
    }

    /**
     * Finds and displays a Badanie entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsMainBundle:Badanie', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Badanie entity.');
        }

        $today = new \DateTime();
        $news = $em->createQueryBuilder()
            ->select('a')
            ->from('LsMainBundle:News', 'a')
            ->where('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today->format('Y-m-d 00:00:00'))
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();

        foreach ($news as $key => $article) {
            $news[$key]->setContentShort(Tools::truncateWord($article->getContentShort(), 120, '...'));
        }

        return $this->render('LsMainBundle:Badanie:show.html.twig', array(
            'entity' => $entity,
            'news' => $news
        ));
    }

}
