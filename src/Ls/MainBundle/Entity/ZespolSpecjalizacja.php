<?php

namespace Ls\MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ZespolSpecjalizacja
 *
 * @ORM\Entity
 * @ORM\Table(name="zespol_specjalizacja")
 */
class ZespolSpecjalizacja {

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Zespol",
     *   mappedBy="specjalizacja"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $zespol;

    /**
     * Constructor
     */
    public function __construct() {
        $this->zespol = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ZespolSpecjalizacja
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return ZespolSpecjalizacja
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Add zespol
     *
     * @param \Ls\MainBundle\Entity\Zespol $zespol
     * @return Gallery
     */
    public function addZespol(Zespol $zespol)
    {
        $this->zespol[] = $zespol;

        return $this;
    }

    /**
     * Remove zespol
     *
     * @param \Ls\MainBundle\Entity\Zespol $zespol
     */
    public function removeZespol(Zespol $zespol)
    {
        $this->zespol->removeElement($zespol);
    }

    /**
     * Get zespol
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZespol()
    {
        return $this->zespol;
    }

    public function __toString() {
        if (is_null($this->getName())) {
            return 'NULL';
        }
        return $this->getName();
    }

}