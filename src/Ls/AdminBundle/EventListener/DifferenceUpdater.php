<?php

namespace Ls\AdminBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\MainBundle\Entity\Difference;
use Ls\MainBundle\Utils\Tools;

class DifferenceUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'postPersist',
            'preUpdate',
            'postUpdate',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Difference) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
            if (null === $entity->getArrangement()) {
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsMainBundle:Difference', 'c')
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Difference) {
            $entity->setUpdatedAt(new \DateTime());

            if (null === $entity->getArrangement()) {
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsMainBundle:Difference', 'c')
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
    }
}