<?php

namespace Ls\AdminBundle\Controller;

use Ls\AdminBundle\Form\SectionType;
use Ls\MainBundle\Entity\Section;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class SectionController extends Controller {
    private $pager_limit_name = 'admin_section_pager_limit';

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $qb = $em->createQueryBuilder();
        $query = $qb->select('e')
            ->from('LsMainBundle:Section', 'e')
            ->where($qb->expr()->isNull('e.page'))
            ->orderBy('e.arrangement', 'asc')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit
        );
        $entities->setTemplate('LsAdminBundle::paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_section'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Logotypy w stopce', $this->get('router')->generate('ls_admin_section'));

        return $this->render('LsAdminBundle:Section:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
        ));
    }

    public function newAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $entity = new Section();

        $form = $this->createForm(new SectionType(), $entity, array(
            'action' => $this->generateUrl('ls_admin_section_new'),
            'method' => 'POST'
        ));
        $form->add('submit', 'submit', array('label' => 'Zapisz'));
        $form->add('submit_and_list', 'submit', array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', 'submit', array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            if (null !== $entity->getSection()) {
                $sSectionName = uniqid('section-image-') . '.' . $entity->getSection()->guessExtension();
                $entity->setPhoto($sSectionName);
                $entity->upload(true);
            }
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie pliku zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_section_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_section'));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_section_new'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Logotypy w stopce', $this->get('router')->generate('ls_admin_section'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_section_new'));

        return $this->render('LsAdminBundle:Section:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $entity = $em->getRepository('LsMainBundle:Section')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Section entity.');
        }

        $form = $this->createForm(new SectionType(), $entity, array(
            'action' => $this->generateUrl('ls_admin_section_edit', array('id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', 'submit', array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', 'submit', array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            if (null !== $entity->getSection()) {
                $sSectionName = uniqid('section-image-') . '.' . $entity->getSection()->guessExtension();
                $entity->setPhoto($sSectionName);
                $entity->upload(true);
            }
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja logotypu zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_section_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_section'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
        $breadcrumbs->addItem('Logotypy w stopce', $this->get('router')->generate('ls_admin_section'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_section_edit', array('id' => $entity->getId())));

        return $this->render('LsAdminBundle:Section:edit.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsMainBundle:Section')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Section entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie pliku zakończone sukcesem.');

        return new Response('OK');
    }

    private function getMaxKolejnosc() {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $query = $qb->select('COUNT(c.id)')
            ->from('LsMainBundle:Section', 'c')
            ->where($qb->expr()->isNull('c.page'))
            ->getQuery();

        $total = $query->getSingleScalarResult();
        return $total + 1;
    }

    public function moveDownAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsMainBundle:Section')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Section entity.');
        }

        $max = $this->getMaxKolejnosc();
        $old_kolejnosc = $entity->getArrangement();
        $new_kolejnosc = $old_kolejnosc + 1;
        if ($new_kolejnosc < $max) {
            $em = $this->getDoctrine()->getManager();
            $qb = $em->createQueryBuilder();
            $query = $qb->select('c.id')
                ->from('LsMainBundle:Section', 'c')
                ->where('c.arrangement = :arrangement')
                ->andWhere($qb->expr()->isNull('c.page'))
                ->setParameter('arrangement', $new_kolejnosc)
                ->getQuery();

            $old_entity_id = $query->getSingleScalarResult();
            $old_entity = $em->getRepository('LsMainBundle:Section')->findOneById($old_entity_id);
            $old_entity->setArrangement(0);
            $em->persist($old_entity);
            $em->flush();
            $entity->setArrangement($new_kolejnosc);
            $em->persist($entity);
            $em->flush();
            $old_entity->setArrangement($old_kolejnosc);
            $em->persist($old_entity);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->add('success', 'Przeniesienie logotypu do dołu zakończone sukcesem.');

        return new Response('OK');
    }

    public function moveUpAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsMainBundle:Section')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Section entity.');
        }

        $old_kolejnosc = $entity->getArrangement();
        $new_kolejnosc = $old_kolejnosc - 1;
        if ($new_kolejnosc > 0) {
            $em = $this->getDoctrine()->getManager();
            $qb = $em->createQueryBuilder();
            $query = $qb->select('c.id')
                ->from('LsMainBundle:Section', 'c')
                ->where('c.arrangement = :arrangement')
                ->andWhere($qb->expr()->isNull('c.page'))
                ->setParameter('arrangement', $new_kolejnosc)
                ->getQuery();

            $old_entity_id = $query->getSingleScalarResult();
            $old_entity = $em->getRepository('LsMainBundle:Section')->findOneById($old_entity_id);
            $old_entity->setArrangement(0);
            $em->persist($old_entity);
            $em->flush();
            $entity->setArrangement($new_kolejnosc);
            $em->persist($entity);
            $em->flush();
            $old_entity->setArrangement($old_kolejnosc);
            $em->persist($old_entity);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->add('success', 'Przeniesienie logotypu do góry zakończone sukcesem.');

        return new Response('OK');
    }

    public function batchAction() {
        $request = $this->get('request');

        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_admin_homepage'));
            $breadcrumbs->addItem('Logotypy', $this->get('router')->generate('ls_admin_section'));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_section_batch'));

            return $this->render('LsAdminBundle:Section:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_section'));
        }
    }

    public function batchExecuteAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $batchSize = 50;
                    $index = 0;
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsMainBundle:Section', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        if (($index % $batchSize) === 0) {
                            $em->flush();
                            $em->clear();
                        }
                        ++$index;
                    }
                    $em->flush();
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_section'));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_section'));
        }
    }

    public function setLimitAction() {
        $request = $this->get('request');
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
}
