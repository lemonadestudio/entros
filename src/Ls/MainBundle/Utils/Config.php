<?php

namespace Ls\MainBundle\Utils;

use Doctrine\ORM\EntityManager;
use Ls\MainBundle\Entity\Opinia;
use Ls\MainBundle\Entity\Setting;
use Ls\MainBundle\Form\OpiniaType;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Config {

    protected $em;
    protected $repo;
    protected $container;
    protected $settings  = array();
    protected $is_loaded = false;

    public function __construct(EntityManager $em, ContainerInterface $container) {
        $this->em = $em;
        $this->container = $container;
    }

    public function get($name, $default) {
        if (array_key_exists($name, $this->settings)) {
            return $this->settings[$name];
        }
        return $default;
    }

    public function all() {
        $settings = array();

        if ($this->is_loaded) {
            return $this->settings;
        }

        foreach ($this->getRepo()->findAll() as $setting) {
            $settings[$setting->getLabel()] = $setting->getValue();
        }

        $this->settings = $settings;
        $this->is_loaded;

        return $settings;
    }

    public function opinia() {
        $entities = $this->em->createQueryBuilder()
            ->select('b')
            ->from('LsMainBundle:Opinia', 'b')
            ->where('b.accepted = 1')
            ->orderBy('b.created_at', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        if(count($entities) > 0) {
            $entity = clone $entities[0];
            $entity->setContent(Tools::truncateWord($entity->getContent(), 100, '...'));
        } else {
            $entity = null;
        }

        return $entity;
    }

    public function icons() {
        $qb = $this->em->createQueryBuilder();
        $entities = $qb->select('b')
            ->from('LsMainBundle:Icon', 'b')
            ->where($qb->expr()->isNull('b.page'))
            ->orderBy('b.arrangement', 'asc')
            ->getQuery()
            ->getResult();

        return $entities;
    }

    protected function getRepo() {
        if ($this->repo === null) {
            $this->repo = $this->em->getRepository(get_class(new Setting()));
        }

        return $this->repo;
    }

}
