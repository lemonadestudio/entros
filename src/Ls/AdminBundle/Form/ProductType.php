<?php

namespace Ls\AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', 'textarea', array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('slug', null, array(
            'label' => 'Końcówka adresu URL'
        ));
        $builder->add('content_short', null, array(
            'label' => 'Opis pod tytułem',
            'attr' => array(
                'rows' => 5
            )
        ));
       $builder->add('gallery', 'entity', array(
           'label' => 'Galeria',
           'attr' => array('class' => 'form-control'),
           'class' => 'LsMainBundle:Gallery',
           'required' => false,
           'query_builder' => function (EntityRepository $er) {
               return $er->createQueryBuilder('g')
                   ->where('g.attachable = 1');
           }
       ));
        // $builder->add('content_short_generate', null, array(
        //     'label' => 'Automatycznie generuj krótką treść'
        // ));
        $builder->add('content', null, array(
            'label' => 'Treść'
        ));
        $builder->add('seo_generate', null, array(
            'label' => 'Generuj automatycznie'
        ));
        $builder->add('seo_title', null, array(
            'label' => 'Tag "title"'
        ));
        $builder->add('seo_keywords', 'textarea', array(
            'label' => 'Meta "keywords"'
        ));
        $builder->add('seo_description', 'textarea', array(
            'label' => 'Meta "description"',
            'attr' => array(
                'rows' => 3
            )
        ));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $product = $event->getData();
            $form = $event->getForm();

            if (!$product || null === $product->getId()) {
                $form->add('file', 'file', array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wybierz zdjęcie'
                        )),
                        new Image(array(
                            'minWidth' => 275,
                            'minHeight' => 231,
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż 288px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 620px',
                        ))
                    )
                ));
            } else {
                $form->add('file', 'file', array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new Image(array(
                            'minWidth' => 275,
                            'minHeight' => 231,
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż 288px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 620px',
                        ))
                    )
                ));
            }
        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\MainBundle\Entity\Product',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_product';
    }
}
