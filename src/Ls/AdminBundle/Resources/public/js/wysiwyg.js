$(function () {
    fckconfig_common = {
        skin: 'BootstrapCK-Skin',
        contentsCss: [
            'http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic&subset=latin,latin-ext',
            '/bundles/lsmain/css/style.css'
        ],
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny', element: '', attributes: {'class': ''}},
            {name: 'Nagłówek', element: 'h1', attributes: {'class': ''}},
            {name: 'Pomarańczowy', element: 'span', attributes: {'class': 'orange'}},
            {name: 'Ciemny', element: 'span', attributes: {'class': 'dark'}}
        ],
        filebrowserBrowseUrl: '/bundles/lsadmin/kcfinder/browse.php?type=files',
        filebrowserImageBrowseUrl: '/bundles/lsadmin/kcfinder/browse.php?type=images',
        filebrowserFlashBrowseUrl: '/bundles/lsadmin/kcfinder/browse.php?type=flash',
        filebrowserUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=flash'
    };

    fckconfig = jQuery.extend(false, {
        height: '400px',
        width: 'auto'
    }, fckconfig_common);

    $('.wysiwyg').ckeditor(fckconfig);

    fckconfig_basic = {
        skin: 'BootstrapCK-Skin',
        contentsCss: [
            'http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic&subset=latin,latin-ext',
            '/bundles/lsmain/css/editor.css'
        ],
        bodyClass: 'editor',
        toolbar: [
            ['Bold', 'Italic', 'Underline', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BulletedList', '-', 'Source', '-', 'Link', 'Unlink', '-', 'Image']
        ],
        filebrowserBrowseUrl: '/bundles/lsadmin/kcfinder/browse.php?type=files',
        filebrowserImageBrowseUrl: '/bundles/lsadmin/kcfinder/browse.php?type=images',
        filebrowserFlashBrowseUrl: '/bundles/lsadmin/kcfinder/browse.php?type=flash',
        filebrowserUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=flash'
    };

    fckconfig_base = jQuery.extend(false, {
        height: '400px',
        width: 'auto'
    }, fckconfig_basic);

    $('.wysiwyg-basic').ckeditor(fckconfig_base);

    fckconfig_news = {
        skin: 'BootstrapCK-Skin',
        contentsCss: [
            'http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic&subset=latin,latin-ext',
            '/bundles/lsmain/css/editor-news.css'
        ],
        bodyClass: 'editor',
        toolbar: [
            ['Source', '-', 'Bold', 'Italic', 'Underline', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BulletedList', '-', 'Link', 'Unlink', '-', 'Image']
        ],
        filebrowserBrowseUrl: '/bundles/lsadmin/kcfinder/browse.php?type=files',
        filebrowserImageBrowseUrl: '/bundles/lsadmin/kcfinder/browse.php?type=images',
        filebrowserFlashBrowseUrl: '/bundles/lsadmin/kcfinder/browse.php?type=flash',
        filebrowserUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=flash',
        height: '400px',
        width: 'auto'
    };

    $('.wysiwyg-news').ckeditor(fckconfig_news);
});

