<?php

namespace Ls\AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class SectionProductType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('description', 'hidden', array(
            'label' => ''
        ));
       $builder->add('gallery', 'entity', array(
           'label' => 'Galeria',
           'attr' => array('class' => 'form-control'),
           'class' => 'LsMainBundle:Gallery',
           'required' => false,
           'query_builder' => function (EntityRepository $er) {
               return $er->createQueryBuilder('g')
                   ->where('g.attachable = 1');
           }
       ));
        $builder->add('content', 'textarea', array(
            'label' => 'Treść'
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\MainBundle\Entity\Section',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_section';
    }
}
