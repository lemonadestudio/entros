<?php

namespace Ls\AdminBundle\Utils;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Sitemap {
    private $container;
    private $router;
    private $xml;
    private $xmlRoot;
    private $em;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getManager();
        $this->router = $this->container->get('router');
        $this->xml = null;
    }

    public function generate() {
        $this->xml = new \SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' standalone='yes'?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"></urlset>");

        $url = $this->container->get('router')->generate('ls_main_homepage', array(), true);
        $child = $this->xml->addChild('url');
        $child->addChild('loc', $url);

        $this->oferta();
        $this->aktualnosci();

        $filename = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'sitemap.xml';

        $dom = new \DOMDocument('1.0');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($this->xml->asXML());
        $dom->save($filename);
    }

    public function oferta() {
        $items = $this->em->createQueryBuilder()
            ->select('e')
            ->from('LsMainBundle:Product', 'e')
            ->orderBy('e.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        $url = $this->container->get('router')->generate('ls_main_oferta', array(), true);
        $child = $this->xml->addChild('url');
        $child->addChild('loc', $url);

        foreach ($items as $item) {
            $url = $this->container->get('router')->generate('ls_main_oferta_show', array('slug' => $item->getSlug()), true);
            $child = $this->xml->addChild('url');
            $child->addChild('loc', $url);
            if ($item->getUpdatedAt() != null) {
                $child->addChild('lastmod', $item->getUpdatedAt()->format(\DateTime::ATOM));
            }
        }
    }

    public function aktualnosci() {
        $items = $this->em->createQueryBuilder()
            ->select('e')
            ->from('LsMainBundle:News', 'e')
            ->orderBy('e.published_at', 'ASC')
            ->getQuery()
            ->getResult();

        $url = $this->container->get('router')->generate('ls_main_news', array(), true);
        $child = $this->xml->addChild('url');
        $child->addChild('loc', $url);

        foreach ($items as $item) {
            $url = $this->container->get('router')->generate('ls_main_news_show', array('slug' => $item->getSlug()), true);
            $child = $this->xml->addChild('url');
            $child->addChild('loc', $url);
            if ($item->getUpdatedAt() != null) {
                $child->addChild('lastmod', $item->getUpdatedAt()->format(\DateTime::ATOM));
            }
        }
    }
}
