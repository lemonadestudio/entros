<?php

namespace Ls\AdminBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\MainBundle\Entity\Photo;
use Ls\MainBundle\Utils\Tools;

class GalleryPhotoUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'postPersist',
            'preUpdate',
            'postUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Photo) {
            if (null === $entity->getArrangement()) {
                $qb = $em->createQueryBuilder();
                $qb->select('COUNT(c.id)');
                $qb->from('LsMainBundle:Photo', 'c');
                if (null === $entity->getGallery()) {
                    $qb->where($qb->expr()->isNull('c.gallery'));
                } else {
                    $qb->where('c.gallery = :page');
                    $qb->setParameter('page', $entity->getGallery()->getId());
                }
                $query = $qb->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Photo) {
            if (null === $entity->getArrangement()) {
                $qb = $em->createQueryBuilder();
                $qb->select('COUNT(c.id)');
                $qb->from('LsMainBundle:Photo', 'c');
                if (null === $entity->getGallery()) {
                    $qb->where($qb->expr()->isNull('c.page'));
                } else {
                    $qb->where('c.gallery = :page');
                    $qb->setParameter('page', $entity->getGallery()->getId());
                }
                $query = $qb->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Photo) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();

                $qb = $em->createQueryBuilder();
                $qb->select('c');
                $qb->from('LsMainBundle:Photo', 'c');
                if (null === $entity->getGallery()) {
                    $qb->where($qb->expr()->isNull('c.gallery'));
                } else {
                    $qb->where('c.gallery = :page');
                    $qb->setParameter('page', $entity->getGallery()->getId());
                }
                $qb->andWhere('c.arrangement > :arrangement');
                $qb->setParameter('arrangement', $arrangement);
                $query = $qb->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['type'] = 'Photo';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
        }
    }
}