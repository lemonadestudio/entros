<?php

namespace Ls\MainBundle\Utils;

use Ls\CmsBundle\Entity\MenuItem;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;

class Tools {

    protected $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    static public function truncateWord($string, $limit, $delimiter) {
        $new = preg_replace('/\s+?(\S+)?$/', '', mb_substr($string . ' ', 0, $limit));

        if (strlen($string) > $limit) {
            $new .= $delimiter;
        }

        return $new;
    }

    static public function thumbName($filename, $appendix) {
        $temp = explode('.', $filename);
        $ext = end($temp);
        $thumbname = substr($filename, 0, strlen($filename) - (strlen($ext) + 1)) . $appendix . "." . $ext;
        return $thumbname;
    }

    static public function dump($value) {
        echo '<pre style="color: red;">';
        var_dump($value);
        echo '</pre>';
    }

    static public function print_r($value) {
        echo '<pre style="color: red;">';
        print_r($value);
        echo '</pre>';
    }

    static function changeSecToTime($time) {
        $hour = floor($time / 3600);
        $min = floor(($time - ($hour * 3600)) / 60);
        $sec = $time - ($hour * 3600) - ($min * 60);
        if ($min == 0) {
            $min = '00';
        }

        if ($min < 10 && $min > 0) {
            $min = implode('', array('0', $min));
        }

        if ($sec == 0) {
            $sec = '00';
        }

        if ($sec < 10 && $sec > 0) {
            $sec = implode('', array('0', $sec));
        }

        return $hour . ':' . $min . ':' . $sec;
    }

    static public function strip_pl($str) {
        $search = array('Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ó', 'Ś', 'Ź', 'Ż', 'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ź', 'ż');
        $replace = array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z', 'a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z');
        return str_replace($search, $replace, $str);
    }

    static public function slugify($text) {
        // replace all non letters or digits by -
        $text = preg_replace('/\W+/', '-', $text);

        // trim and lowercase
        $text = mb_convert_case(trim($text, '-'), MB_CASE_LOWER, 'UTF-8');

        return $text;
    }

    static public function generateGuid() {
        if (function_exists('com_create_guid')) {
            return trim(com_create_guid(), '{}');
        } else {
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45); // "-"
            $uuid = substr($charid, 0, 8) . $hyphen
                . substr($charid, 8, 4) . $hyphen
                . substr($charid, 12, 4) . $hyphen
                . substr($charid, 16, 4) . $hyphen
                . substr($charid, 20, 12);
            return $uuid;
        }
    }

    static public function getFormErrors(Form $form) {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            if (!$form->isRoot()) {
                $errors[] = $error->getMessage();
            }
        }

        if (count($errors) > 0) {
            $errors = implode(', ', $errors);
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = Tools::getFormErrors($child);
            }
        }

        return $errors;
    }

    public function createBreadcrumpsFromMenu($route, $title = null) {
        $em = $this->container->get('doctrine')->getManager();
        $router = $this->container->get('router');

        $breadcrumbs = $this->container->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Strona głowna', $this->container->get('router')->generate('lscms_homepage'));

        $menuitem = $em->createQueryBuilder()
            ->select('m')
            ->from('LsCmsBundle:MenuItem', 'm')
            ->where('m.location = :location')
            ->andWhere('m.type = :type')
            ->andWhere('m.route = :route')
            ->setParameter('location', 'menu_top')
            ->setParameter('type', 'route')
            ->setParameter('route', $route)
            ->getQuery()
            ->getOneOrNullResult();

        $items = array();
        if (null != $menuitem) {
            while (null !== $menuitem->getParent()) {
                $item = array(
                    'title' => mb_convert_case($menuitem->getTitle(), MB_CASE_TITLE, 'UTF-8'),
                    'object' => $menuitem
                );
                if (null !== $title) {
                    $item['title'] = $title;
                    $title = null;
                }
                $items[] = $item;
                $menuitem = $menuitem->getParent();
            }
            if (null != $menuitem) {
                $item = array(
                    'title' => mb_convert_case($menuitem->getTitle(), MB_CASE_TITLE, 'UTF-8'),
                    'object' => $menuitem
                );
                if (null !== $title) {
                    $item['title'] = $title;
                    $title = null;
                }
                $items[] = $item;
            }
        }

        $items = array_reverse($items);
        foreach ($items as $item) {
            $uri = '';
            switch ($item['object']->getType()) {
                case 'url':
                    $uri = $item['object']->getUrl();
                    break;

                case 'route':
                    if ($item['object']->getRoute()) {
                        $route = $router->getRouteCollection()->get($item['object']->getRoute());
                        if ($route) {
                            $parameters = array();
                            foreach ($item['object']->getRouteParametersArray() as $k => $param) {
                                if (preg_match('@\{' . $k . '\}@', $route->getPath())) {
                                    $parameters[$k] = $param;
                                }
                            }

                            try {
                                $uri = $router->generate($item['object']->getRoute(), $parameters);
                            } catch (\Exception $e) {
                                // return false;
                            }
                        }
                    }
                    break;

                case 'button':
                    $uri = '#';
                    break;

                default:
                    break;
            }
            $breadcrumbs->addItem($item['title'], $uri);
        }
        return $breadcrumbs;
    }

    public function generateUri($item) {
        $router = $this->container->get('router');
        $result = array(
            'uri' => null,
            'itemAttributes' => array(),
            'linkAttributes' => array(),
        );

        switch ($item->getType()) {
            case 'url':
                $result['uri'] = $item->getUrl();
                if ($item instanceof MenuItem) {
                    if (count($item->getChildren()) > 0) {
                        $result['itemAttributes']['class'] = 'has-submenu';
                        $result['linkAttributes']['class'] = 'has-submenu';
                    }
                }
                break;

            case 'route':
                if ($item->getRoute()) {
                    $route = $router->getRouteCollection()->get($item->getRoute());
                    if ($route) {
                        $parameters = array();
                        foreach ($item->getRouteParametersArray() as $k => $param) {
                            if (preg_match('@\{' . $k . '\}@', $route->getPath())) {
                                $parameters[$k] = $param;
                            }
                        }

                        try {
                            $result['uri'] = $router->generate($item->getRoute(), $parameters);
                        } catch (\Exception $e) {
                            // return false;
                        }
                    }
                }
                $class = array();
                if ($item instanceof MenuItem) {
                    if (count($item->getChildren()) > 0) {
                        $class[] = 'has-submenu';
                    }
                    if ($item->getRoute() == 'lscms_homepage') {
                        $class[] = 'home';
                    }
                }
                if (count($class) > 0) {
                    $result['itemAttributes']['class'] = implode(' ', $class);
                }
                break;

            case 'button':
                $result['uri'] = '#';
                if ($item instanceof MenuItem) {
                    if (count($item->getChildren()) > 0) {
                        $result['itemAttributes']['class'] = 'has-submenu';
                        $result['linkAttributes']['class'] = 'has-submenu';
                    }
                }
                $result['linkAttributes']['onclick'] = 'return ' . $item->getOnClick() . ';';
                break;

            default:
                break;
        }

        return $result;
    }
}
